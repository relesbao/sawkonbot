﻿using SawkonBot.Utils;
using SharpDX;
using System;

namespace SawkonBot.Game.Engine
{
    struct Player
    {
        public static Int64 nameOffset = 0x40;
        public static Int64 teamIdOffset = 0x13CC;
        public static Int64 characterOffset = 0x14B0;
        public static Int64 vehicleOffset = 0x14C0;
        public static Int64 soldierOffset = 0x14D0;
        public static Int64 spectatorOffset = 0x13C9;
        public static Int64 attachedEntryOffset = 0x14C8;
    }

    struct Soldier
    {
        
        public static Int64 poseTypeOffset = 0x04F0;
        public static Int64 occludedOffset = 0x05B1;
        public static Int64 healthOffset = 0x0140;
        public static Int64 soldierPredictionOffset = 0x0490;
        public static Int64 yawOffset = 0x04D8;
        public static Int64 ragdollOffset = 0x580;
        public static Int64 weaponsComponentOffset = 0x0570;
        public static Int64 breathControlComponentOffset = 0x0588;
    }

    struct ClientVehicle
    {
        public static Int64 childrenAABBOffset = 0x0250;
        public static Int64 dinamycPhisycsEntityOffset = 0x0238;
        public static Int64 clientVehicleHealthComponentOffset = 0x0140;
        public static Int64 vehicleEntityDataOffset = 0x30;
    }

    struct VehicleEntityData
    {
        public static Int64 nameSidOffset = 0x0248;
        public static Int64 frontHealthZoneOffset = 0x0148;
    }

    struct DinamycPhisycsEntity
    {
        public static Int64 phisycsEntityOffset = 0xA0;
        public static Int64 phisycsTransformOffset = 0x00;
    }

    struct BreathControl
    {
        public static Int64 BreathControlTimerOffset = 0x0044;
        public static Int64 BreathControlMultiplierOffset = 0x0048;
        public static Int64 BreathControlPenaltyTimerOffset = 0x004C;
        public static Int64 BreathControlPenaltyMultiplierOffset = 0x0050;
        public static Int64 BreathControlActiveOffset = 0x0054;
        public static Int64 BreathControlInputOffset = 0x0058;
    }

    public struct Health
    {
        public static Int64 healthOffset = 0x0020;        
        public static Int64 maxHealthOffset = 0x0024;     
        public static Int64 vehicleHealthOffset = 0x0038;
    }

    public struct SoldierPrediction
    {
        public static Int64 positionOffset = 0x0030;
        public static Int64 velocityOffset = 0x0050;
    }

    public struct SoldierWeaponsComponent
    {
        public static Int64 activeSlotOffset = 0x0A98;
        public static Int64 handlerOffset = 0x0890;
    }

    public struct SoldierWeapon
    {
        public static Int64 weaponFiringOffset = 0x49C0;
    }

    public struct WeaponFiring
    {
        public static Int64 weaponSwayDataOffset = 0x0078;
        public static Int64 weaponFiringData = 0x128;
    }

    public struct WeaponFiringData
    {
        public static Int64 shotConfigDataOffset = 0x0010;
    }

    public struct ShotConfigData
    {
        public static Int64 positionOffset = 0x0060;
        public static Int64 initialSpeedOffset = 0x0088;
        public static Int64 bulletEntityDataOffset = 0x00B0;
    }

    public struct BulletEntityData
    {
        public static Int64 gravityOffset = 0x0130;
        public static Int64 startDamageOffset = 0x0154;
        public static Int64 endDamageOffset = 0x0158;
    }

    public struct AimAssist
    {
        public static Int64 m_yaw = 0x0014;   // FLOAT
        public static Int64 m_pitch = 0x0018; // FLOAT
    }

    public struct WeaponSwayData
    {
        public static Int64 gunSwayDataOffset = 0x08;
    }

    public struct GunSwayData
    {
        // No recoil
        public static Int64 shootingRecoilDecreaseScaleOffset = 0x0440;
        public static Int64 firstShotRecoilMultiplierOffset = 0x0444;
        // No spread
        public static Int64 deviationScaleFactorZoomOffset = 0x0430;
        public static Int64 gameplayDeviationScaleFactorZoomOffset = 0x0434;
        public static Int64 deviationScaleFactorNoZoomOffset = 0x0438;
        public static Int64 gameplayDeviationScaleFactorNoZoomOffset = 0x043C;
    }
}
