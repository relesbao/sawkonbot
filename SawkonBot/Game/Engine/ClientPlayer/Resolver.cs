﻿using SawkonBot.Utils;
using SharpDX;
using System;
using Matrix4x4 = System.Numerics.Matrix4x4;

namespace SawkonBot.Game.Engine.ClientPlayer
{
    public class Resolver
    {

        public static string GetName(Int64 Offset)
        {
            if (Memory.IsValid(Offset))
            {
                return Memory.ReadString(Offset + Engine.Player.nameOffset, 10);
            }
            return "null";
        }

        public static Int64 GetVehicle(Int64 Offset)
        {
            if (Memory.IsValid(Offset))
            {
                Int64 soldierInsideVehicle = Memory.Read<Int64>(Memory.Read<Int64>(Offset + Player.characterOffset)) - sizeof(Int64);
                if (Memory.IsValid(soldierInsideVehicle))
                {
                    return Memory.Read<Int64>(Offset + Player.vehicleOffset);
                }
            }
            return 0;
        }

        public static bool IsDriver(Int64 Offset)
        {
            if (Memory.IsValid(Offset))
            {
                return Memory.Read<Int32>(Offset + Player.attachedEntryOffset) == 0;
            }
            return false;
        }

        public static Int64 GetSoldier(Int64 Offset, Int64 VehicleOffset)
        {
            if (Memory.IsValid(VehicleOffset))
            {
                // Read the soldier inside the vehicle
                return Memory.Read<Int64>(Memory.Read<Int64>(Offset + Player.characterOffset)) - sizeof(Int64);
            }
            return Memory.ReadInt64(Offset + Engine.Player.soldierOffset);
        }

        public static void GetBulletSpeedAndGravity(Int64 weaponFiring, out float bulletSpeed, out float bulletGravity)
        {
            bulletSpeed = bulletGravity = 0;
            if (Memory.IsValid(weaponFiring))
            {
                Int64 weaponFiringData = Memory.Read<Int64>(weaponFiring + WeaponFiring.weaponFiringData);
                if (Memory.IsValid(weaponFiringData))
                {
                    Int64 shotConfigData = Memory.Read<Int64>(weaponFiringData + WeaponFiringData.shotConfigDataOffset);
                    if (Memory.IsValid(shotConfigData))
                    {
                        Int64 bulletEntityData = Memory.Read<Int64>(shotConfigData + ShotConfigData.bulletEntityDataOffset);
                        if (Memory.IsValid(bulletEntityData))
                        {
                            bulletGravity = Memory.Read<float>(bulletEntityData + BulletEntityData.gravityOffset);
                            bulletSpeed = Memory.Read<float>(shotConfigData + ShotConfigData.initialSpeedOffset);
                        }
                    }
                }
            }
        }

        public static int GetPose(Int64 Soldier)
        {
            if (Memory.IsValid(Soldier))
            {
                return Memory.Read<int>(Soldier + Engine.Soldier.poseTypeOffset);
            }
            return 0;
        }
        public static bool GetOccluded(Int64 Soldier)
        {
            if (Memory.IsValid(Soldier))
            {
                Byte occluded = Memory.Read<byte>(Soldier + Engine.Soldier.occludedOffset);
                return occluded != 0;
            }
            return true;
        }

        internal static bool IsSpectator(Int64 Offset)
        {
            return Convert.ToBoolean(Memory.Read<Byte>(Offset + Engine.Player.spectatorOffset));
        }

        public static int GetTeam(Int64 Offset)
        {
            if (Memory.IsValid(Offset))
            {
                return Memory.Read<int>(Offset + Engine.Player.teamIdOffset);
            }
            return 0;
        }

        public static Vector3 GetPosition(Int64 Soldier)
        {
            if (Memory.IsValid(Soldier))
            {
                Int64 prediction = Memory.ReadInt64(Soldier + Engine.Soldier.soldierPredictionOffset);
                if (Memory.IsValid(prediction))
                {
                    return Memory.Read<Vector3>(prediction + Engine.SoldierPrediction.positionOffset);
                }
            }
            return new Vector3(0, 0, 0);
        }

        public static Vector3 GetVelocity(Int64 Soldier)
        {
            if (Memory.IsValid(Soldier))
            {
                Int64 prediction = Memory.ReadInt64(Soldier + Engine.Soldier.soldierPredictionOffset);
                if (Memory.IsValid(prediction))
                {
                    return Memory.Read<Vector3>(prediction + Engine.SoldierPrediction.velocityOffset);
                }
            }
            return new Vector3(0, 0, 0);
        }

        public static float GetYaw(Int64 Soldier)
        {
            if (Memory.IsValid(Soldier))
            {
                return Memory.Read<float>(Soldier + Engine.Soldier.yawOffset);
            }
            return 0;
        }

        public static void GetHealth(Int64 Soldier, out float _health, out float _maxHealth)
        {
            _health = 0;
            _maxHealth = 0;
            if (Memory.IsValid(Soldier))
            {
                Int64 health = Memory.ReadInt64(Soldier + Engine.Soldier.healthOffset);
                if (Memory.IsValid(health))
                {
                    _health = Memory.Read<float>(health + Health.healthOffset);
                    _maxHealth = Memory.Read<float>(health + Health.maxHealthOffset);
                }
            }
        }

        public static Bone GetBone(Int64 Soldier)
        {
            Bone bone = new Bone();
            Int64 Ragdoll = GetRagdoll(Soldier);
            if (Memory.IsValid(Ragdoll))
            {
                bone.Head = GetBoneById(Ragdoll, 104);
                bone.Neck = GetBoneById(Ragdoll, 142);
                bone.LeftShoulder = GetBoneById(Ragdoll, 9);
                bone.RightShoulder = GetBoneById(Ragdoll, 109);
                bone.LeftElbow = GetBoneById(Ragdoll, 11);
                bone.RightElbow = GetBoneById(Ragdoll, 111);
                bone.LeftHand = GetBoneById(Ragdoll, 15);
                bone.RightHand = GetBoneById(Ragdoll, 115);
                bone.Spine = GetBoneById(Ragdoll, 5);
                bone.LeftKnee = GetBoneById(Ragdoll, 188);
                bone.RightKnee = GetBoneById(Ragdoll, 197);
                bone.LeftFoot = GetBoneById(Ragdoll, 185);
                bone.RightFoot = GetBoneById(Ragdoll, 199);
            }           
            return bone;
        }

        public static Int64 GetRagdoll(Int64 Soldier)
        {
            if (Memory.IsValid(Soldier))
            {
                return Memory.Read<Int64>(Soldier + Engine.Soldier.ragdollOffset);
            }
            return 0;
        }

        public static Vector3 GetBoneById(Int64 Ragdoll, int boneId)
        {
            Vector3 temp = new Vector3();
            Int64 pQuatTransform = Memory.Read<Int64>(Ragdoll + 0x00A8);
            if (!Memory.IsValid(pQuatTransform)) return temp;
            byte m_ValidTransforms = Memory.Read<Byte>(Ragdoll + 0x00C8);
            if (m_ValidTransforms != 1) return temp;
            temp = Memory.Read<Vector3>(pQuatTransform + (boneId * 0x20));
            return temp;
        }

        public static Int64 GetWeaponComponent(Int64 Soldier)
        {
            if (Memory.IsValid(Soldier))
            {
                return Memory.Read<Int64>(Soldier + Engine.Soldier.weaponsComponentOffset);
            }
            return 0;
        }

        public static Int64 GetWeaponHandler(Int64 WeaponComponent)
        {
            if (Memory.IsValid(WeaponComponent))
            {
                return Memory.Read<Int64>(WeaponComponent + Engine.SoldierWeaponsComponent.handlerOffset);
            }
            return 0;
        }

        public static Int32 GetActiveSlot(Int64 WeaponComponent)
        {
            if (Memory.IsValid(WeaponComponent))
            {
                return Memory.Read<Int32>(WeaponComponent + Engine.SoldierWeaponsComponent.activeSlotOffset);
            }
            return 0;
        }

        public static Int64 GetSoldierWeapon(Int64 WeaponHandler, Int32 ActiveSlot)
        {
            if (Memory.IsValid(WeaponHandler))
            {
                return Memory.Read<Int64>(WeaponHandler + ActiveSlot * 0x08);
            }
            return 0;
        }

        public static Int64 GetWeaponFiring(Int64 SoldierWeapon)
        {
            if (Memory.IsValid(SoldierWeapon))
            {
                return Memory.Read<Int64>(SoldierWeapon + Engine.SoldierWeapon.weaponFiringOffset);
            }
            return 0;
        }

        public static Int64 GetGunSwayData(Int64 WeaponFiring)
        {
            if (Memory.IsValid(WeaponFiring))
            {
                Int64 weaponSwayData = Memory.Read<Int64>(WeaponFiring + Engine.WeaponFiring.weaponSwayDataOffset);
                if (Memory.IsValid(weaponSwayData))
                {
                    return Memory.Read<Int64>(weaponSwayData + Engine.WeaponSwayData.gunSwayDataOffset);
                }
            }
            return 0;
        }

        public static Utils.Math.AxisAlignedBox GetVehicleAABB(Int64 Vehicle)
        {
            if (Memory.IsValid(Vehicle))
            {
                return Memory.ReadAABB(Vehicle + Engine.ClientVehicle.childrenAABBOffset);
            }
            return new Utils.Math.AxisAlignedBox();
        }

        public static Matrix4x4 GetVehicleTransform(Int64 Vehicle)
        {
            if (Memory.IsValid(Vehicle))
            {
                Int64 dinamicPhisycsEntity = Memory.Read<Int64>(Vehicle + Engine.ClientVehicle.dinamycPhisycsEntityOffset);
                if (Memory.IsValid(dinamicPhisycsEntity))
                {
                    Int64 phisycsEntityOffset = Memory.Read<Int64>(dinamicPhisycsEntity + Engine.DinamycPhisycsEntity.phisycsEntityOffset);
                    if (Memory.IsValid(phisycsEntityOffset))
                    {
                        return Memory.ReadMatrix(phisycsEntityOffset + Engine.DinamycPhisycsEntity.phisycsTransformOffset);
                    }
                }
            }
            return new Matrix4x4();
        }

        public static Int64 GetVehicleEntityData(Int64 Vehicle)
        {
            if (Memory.IsValid(Vehicle))
            {
                return Memory.Read<Int64>(Vehicle + Engine.ClientVehicle.vehicleEntityDataOffset);
            }
            return 0;
        }

        public static string GetVehicleName(Int64 VehicleEntityData)
        {
            if (Memory.IsValid(VehicleEntityData))
            {
                string name = Memory.ReadString(Memory.Read<Int64>(VehicleEntityData + Engine.VehicleEntityData.nameSidOffset), 20UL);
                return name.Remove(0, 11);
            }
            return "";
        }

        public static float GetVehicleHealth(Int64 Vehicle)
        {
            if (Memory.IsValid(Vehicle))
            {
                Int64 health = Memory.ReadInt64(Vehicle + Engine.Soldier.healthOffset);
                if (Memory.IsValid(health))
                {
                    return Memory.Read<float>(health + Health.vehicleHealthOffset);
                }
            }
            return 0;
        }

        public static float GetVehicleMaxHealth(Int64 VehicleEntityData)
        {
            if (Memory.IsValid(VehicleEntityData))
            {
                return Memory.Read<float>(VehicleEntityData + Engine.VehicleEntityData.frontHealthZoneOffset);
            }
            return 0;
        }
    }
}
