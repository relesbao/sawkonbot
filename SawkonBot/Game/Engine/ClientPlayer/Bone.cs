﻿using SharpDX;

namespace SawkonBot.Game.Engine.ClientPlayer
{
    public class Bone
    {
        public Vector3 Head = new Vector3();
        public Vector3 Neck = new Vector3();
        public Vector3 LeftShoulder = new Vector3();
        public Vector3 RightShoulder = new Vector3();
        public Vector3 LeftElbow = new Vector3();
        public Vector3 RightElbow = new Vector3();
        public Vector3 LeftHand = new Vector3();
        public Vector3 RightHand = new Vector3();
        public Vector3 Spine = new Vector3();
        public Vector3 LeftKnee = new Vector3();
        public Vector3 RightKnee = new Vector3();
        public Vector3 LeftFoot = new Vector3();
        public Vector3 RightFoot = new Vector3();

        public Bone ConvertToScreen()
        {
            Bone bone = new Bone();
            bone.Head = Math.Conversion.WorldToScreen(this.Head);
            bone.Neck = Math.Conversion.WorldToScreen(this.Neck);
            bone.LeftShoulder = Math.Conversion.WorldToScreen(this.LeftShoulder);
            bone.RightShoulder = Math.Conversion.WorldToScreen(this.RightShoulder);
            bone.LeftElbow = Math.Conversion.WorldToScreen(this.LeftElbow);
            bone.RightElbow = Math.Conversion.WorldToScreen(this.RightElbow);
            bone.LeftHand = Math.Conversion.WorldToScreen(this.LeftHand);
            bone.RightHand = Math.Conversion.WorldToScreen(this.RightHand);
            bone.Spine = Math.Conversion.WorldToScreen(this.Spine);
            bone.LeftKnee = Math.Conversion.WorldToScreen(this.LeftKnee);
            bone.RightKnee = Math.Conversion.WorldToScreen(this.RightKnee);
            bone.LeftFoot = Math.Conversion.WorldToScreen(this.LeftFoot);
            bone.RightFoot = Math.Conversion.WorldToScreen(this.RightFoot);
            return bone;
        }

        public Vector3 GetBoneByName(string name)
        {
            System.Reflection.FieldInfo field = this.GetType().GetField(name);
            Vector3 value = (Vector3)field.GetValue(this);
            return value;
        }
    }
}
