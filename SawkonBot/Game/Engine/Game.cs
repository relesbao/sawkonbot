﻿using System;

namespace SawkonBot.Game.Engine
{
    struct ClientGameContext
    {
        public static Int64 offsetGameContext = 0x1424abd20;
        public static Int64 offsetClientPlayerManager = 0x60;
    }

    struct ClientPlayerManager
    {
        public static Int64 offsetMaxPlayers = 0x0010;
        public static Int64 offsetLocalPlayer = 0x540;
        public static Int64 offsetClientPlayer = 0x548;
    }

    struct Angles
    {
        public static Int64 offset = 0x1421CAEE0;
        public static Int64 ClientSoldierAimingSimulationOffset = 0x4988;
    }

    struct ClientSoldierAimingSimulation
    {
        public static Int64 fpsAimerOffset = 0x0010;
    }

    struct FpsAimer
    {
        public static Int64 YawOffset = 0x0014;
        public static Int64 PitchOffset = 0x0018; 
    }
}
