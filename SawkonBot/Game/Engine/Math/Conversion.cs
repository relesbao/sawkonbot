﻿using SharpDX;
using Matrix4x4 = System.Numerics.Matrix4x4;
using SawkonBot.Utils.Math;

namespace SawkonBot.Game.Engine.Math
{
    class Conversion
    {
        public static bool WorldToScreen(Vector3 _Enemy, out Vector3 _Screen)
        {
            Matrix4x4 viewProj = Game.Context.GetInstance.EngineContext.ViewProjection;
            _Screen = new Vector3(0, 0, 0);
            float ScreenW = (viewProj.M14 * _Enemy.X) + (viewProj.M24 * _Enemy.Y) + (viewProj.M34 * _Enemy.Z + viewProj.M44);

            if (ScreenW < 9.99999974737875E-05)
                return false;

            float ScreenX = (viewProj.M11 * _Enemy.X) + (viewProj.M21 * _Enemy.Y) + (viewProj.M31 * _Enemy.Z + viewProj.M41);
            float ScreenY = (viewProj.M12 * _Enemy.X) + (viewProj.M22 * _Enemy.Y) + (viewProj.M32 * _Enemy.Z + viewProj.M42);

            _Screen.X = Game.Context.GetInstance.EngineContext.CrossX + Game.Context.GetInstance.EngineContext.CrossX * (ScreenX / ScreenW);
            _Screen.Y = Game.Context.GetInstance.EngineContext.CrossY - Game.Context.GetInstance.EngineContext.CrossY * ScreenY / ScreenW;
            _Screen.Z = ScreenW;

            return true;
        }

        public static bool WorldToScreen(Vector3 _Enemy, int _Pose, out Vector3 _Screen)
        {
            Matrix4x4 viewProj = Game.Context.GetInstance.EngineContext.ViewProjection;
            _Screen = new Vector3(0.0f, 0.0f, 0.0f);
            float y = _Enemy.Y;
            if (_Pose == 0)
                y += 1.7f;
            if (_Pose == 1)
                y += 1.15f;
            if (_Pose == 2)
                y += 0.4f;
            float ScreenW = (float)(viewProj.M14 * _Enemy.X + viewProj.M24 * y + (viewProj.M34 * _Enemy.Z + viewProj.M44));
            if (ScreenW < 9.99999974737875E-05)
                return false;
            float ScreenX = (viewProj.M11 * _Enemy.X + viewProj.M21 * y + (viewProj.M31 * _Enemy.Z + viewProj.M41));
            float ScreenY = (viewProj.M12 * _Enemy.X + viewProj.M22 * y + (viewProj.M32 * _Enemy.Z + viewProj.M42));
            _Screen.X = Game.Context.GetInstance.EngineContext.CrossX + Game.Context.GetInstance.EngineContext.CrossX * ScreenX / ScreenW;
            _Screen.Y = Game.Context.GetInstance.EngineContext.CrossY - Game.Context.GetInstance.EngineContext.CrossY * ScreenY / ScreenW;
            _Screen.Z = ScreenW;
            return true;
        }

        public static Vector3 WorldToScreen(Vector3 _Enemy)
        {
            Matrix4x4 viewProj = Game.Context.GetInstance.EngineContext.ViewProjection;
            Vector3 Vector3;
            Vector3 onCross = new Vector3();
            float b14 = viewProj.M14 * _Enemy.X + viewProj.M24 * _Enemy.Y + (viewProj.M34 * _Enemy.Z + viewProj.M44);
            if (b14 >= 0.0001f)
            {
                float b11 = viewProj.M11 * _Enemy.X + viewProj.M21 * _Enemy.Y + (viewProj.M31 * _Enemy.Z + viewProj.M41);
                float b12 = viewProj.M12 * _Enemy.X + viewProj.M22 * _Enemy.Y + (viewProj.M32 * _Enemy.Z + viewProj.M42);
                onCross.X = Game.Context.GetInstance.EngineContext.CrossX + Game.Context.GetInstance.EngineContext.CrossX * b11 / b14;
                onCross.Y = Game.Context.GetInstance.EngineContext.CrossY - Game.Context.GetInstance.EngineContext.CrossY * b12 / b14;
                onCross.Z = b14;
                Vector3 = onCross;
            }
            else
            {
                Vector3 = onCross;
            }
            return Vector3;
        }

        public static AxisAlignedBox GetAABB(Model.Player player)
        {
            AxisAlignedBox aabb = new AxisAlignedBox();
            if (player.Pose == 0) // standing
            {
                aabb.Min = new Vector4(-0.350000f, 0.000000f, -0.350000f, 0);
                aabb.Max = new Vector4(0.350000f, 1.700000f, 0.350000f, 0);
            }
            if (player.Pose == 1) // crouching
            {
                aabb.Min = new Vector4(-0.350000f, 0.000000f, -0.350000f, 0);
                aabb.Max = new Vector4(0.350000f, 1.150000f, 0.350000f, 0);
            }
            if (player.Pose == 2) // prone
            {
                aabb.Min = new Vector4(-0.350000f, 0.000000f, -0.350000f, 0);
                aabb.Max = new Vector4(0.350000f, 0.400000f, 0.350000f, 0);
            }
            return aabb;
        }
    }
}
