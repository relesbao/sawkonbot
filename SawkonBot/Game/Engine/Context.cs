﻿using SawkonBot.Utils;
using System;
using System.Numerics;

namespace SawkonBot.Game.Engine
{
    class Context
    {

        public Int64 GameContext;
        public Int64 PlayerManager;

        public Matrix4x4 ViewProjection { get; set; }

        public Matrix4x4 MatrixInverse { get; set; }

        public int CrossX
        {
            get
            {
                return this.MultiplyWidth / 2;
            }
        }

        public int CrossY
        {
            get
            {
                return this.MultiplyHeight / 2;
            }
        }

        public int MultiplyHeight { get; set; }

        public int MultiplyWidth { get; set; }

        public Int64 pScreen { get; set; }

        private Int64 clientPlayer;

        public Context() {

            GameContext = Memory.ReadInt64(ClientGameContext.offsetGameContext);
            if (Memory.IsValid(GameContext))
            {
                PlayerManager = Memory.ReadInt64(GameContext + ClientGameContext.offsetClientPlayerManager);
                clientPlayer = Memory.ReadInt64(PlayerManager + ClientPlayerManager.offsetClientPlayer);
            }
        }

        public void refresh()
        {
            Int64 GameRenderer = Memory.ReadInt64(0x1424AD330);
            Int64 GameRendererContext = Memory.ReadInt64(GameRenderer + 0x60);
            ViewProjection = Memory.ReadMatrix(GameRendererContext + 0x420);
            MatrixInverse = Memory.ReadMatrix(GameRendererContext + 0x2E0);

            Int64 OFFSET_DXRENDERER = Memory.ReadInt64(0x142572fa0);
            pScreen = (OFFSET_DXRENDERER == 0 ? 0 : Memory.ReadInt64(OFFSET_DXRENDERER + 56));

            Game.RECT pRect = Game.Window.GetControlSize();
            MultiplyWidth = pRect.Right - pRect.Left;
            MultiplyHeight = pRect.Bottom - pRect.Top;
            //MultiplyWidth = (pScreen == 0 ? 0 : Memory.Read<Int32>(this.pScreen + 88));
            //MultiplyHeight = (this.pScreen == 0 ? 0 : Memory.Read<Int32>(this.pScreen + 92));
        }

        public int GetMaxPlayers()
        {
            int maxPlayers = 0;
            if (Memory.IsValid(PlayerManager))
            {
                maxPlayers = Memory.Read<int>(PlayerManager + ClientPlayerManager.offsetMaxPlayers);
            }
            return (maxPlayers > 0) ? maxPlayers: 64+4+4;
        }

        public Int64 GetLocalPlayerOffset()
        {
            if (Memory.IsValid(PlayerManager))
            {
                return Memory.Read<Int64>(PlayerManager + ClientPlayerManager.offsetLocalPlayer);
            }
            return 0;
        }

        public Int64 GetPlayerOffset(uint id)
        {
            if (Memory.IsValid(PlayerManager))
            {                
                if (Memory.IsValid(clientPlayer))
                {
                    return Memory.Read<Int64>(clientPlayer + (id * sizeof(Int64)));
                }
            }
            return 0;
        }
    }
}
