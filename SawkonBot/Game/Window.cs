﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace SawkonBot.Game
{

    [StructLayout(LayoutKind.Sequential)]
    public struct RECT
    {
        public int Left;
        public int Top;
        public int Right;
        public int Bottom;
    }

    class Window
    {

        private static RECT _rect;
        private static DateTime _lastUpdateTime;

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [DllImport("user32.dll")]
        public static extern bool GetWindowRect(IntPtr hWnd, out RECT lpRect);

        public static RECT GetControlSize()
        {
            if(_lastUpdateTime == null)
            {
                _lastUpdateTime = DateTime.Now;                
            }
            if(_lastUpdateTime.AddSeconds(10) < DateTime.Now || (_rect.Left == 0 || _rect.Top == 0))
            {
                GetWindowRect(Process.GetInstance.GetWindowHandler(), out _rect);
                _lastUpdateTime = DateTime.Now;
            }           
            return _rect;
        }
    }
}
