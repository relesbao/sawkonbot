﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SawkonBot.Game.Feature
{
    class End : IFeature, IKeysWatcher
    {
        public bool isEnabled()
        {
            return true;
        }

        public void tick()
        {
            return;
        }

        public void watchKeys()
        {
            if(Utils.Keys.IsKeyDown(Utils.KeyCodes.End)){
                Environment.Exit(0);
            }
        }
    }
}
