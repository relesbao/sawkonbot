﻿namespace SawkonBot.Game.Feature
{
    interface IFeature
    {
        void tick();
        bool isEnabled();
    }
}
