﻿using SawkonBot.View.Helper;
using SharpDX;
using System.Collections.Generic;

namespace SawkonBot.Game.Feature.EspFeature
{
    class Lines : IFeature, IDrawable
    {

        public void draw(DirectxDrawer drawer)
        {
            if (isEnabled() && Context.GetInstance.Player.isAlive())
            {
                var _buffer = Utils.Clone.CloneObject<List<Model.EnemyPlayer>>(Context.GetInstance.Players);

                foreach (Model.EnemyPlayer player in _buffer)
                {
                    if (player.isValid() && player.isAlive() && player.Team != Context.GetInstance.Player.Team)
                    {
                        Color color = Color.Red;
                        if (player.isVisible())
                        {
                            color = Color.Green;
                        }

                        if( (player.inVehicle() && EspUtils.shouldDrawOnVehicle() && player.Vehicle.IsDriver) || EspUtils.shouldDrawOnPlayer())
                        {
                            Vector3 Foot = Game.Engine.Math.Conversion.WorldToScreen(player.Position);
                            drawer.DrawLine(new Vector2(Game.Context.GetInstance.EngineContext.MultiplyWidth / 2, Game.Context.GetInstance.EngineContext.MultiplyHeight), new Vector2((int)Foot.X, (int)Foot.Y), color, 2);
                        }                        
                    }
                }
            }
        }

        public bool isEnabled()
        {
            MenuFeature.BooleanMenuItem Item = (MenuFeature.BooleanMenuItem)MenuFeature.Helper.GetInstance.Items["EspLines"];
            return Item.enabled;
        }

        public void tick()
        {
            return;
        }
    }
}
