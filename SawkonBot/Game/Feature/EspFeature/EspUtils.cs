﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SawkonBot.Game.Feature.EspFeature
{
    class EspUtils
    {
        public static bool shouldDrawOnPlayer()
        {
            MenuFeature.ArrayMenuItem Item = (MenuFeature.ArrayMenuItem)MenuFeature.Helper.GetInstance.Items["EspDrawEntities"];
            if (Item.options[Item.selected] == "All" || Item.options[Item.selected] == "Players")
            {
                return true;
            }
            return false;
        }

        public static bool shouldDrawOnVehicle()
        {
            MenuFeature.ArrayMenuItem Item = (MenuFeature.ArrayMenuItem)MenuFeature.Helper.GetInstance.Items["EspDrawEntities"];
            if (Item.options[Item.selected] == "All" || Item.options[Item.selected] == "Vehicles")
            {
                return true;
            }
            return false;
        }
    }
}
