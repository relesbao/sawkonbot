﻿using System;
using SawkonBot.View.Helper;
using SharpDX;
using System.Collections.Generic;
using System.Reflection;

namespace SawkonBot.Game.Feature.EspFeature
{
    class HealthBar : IFeature, IDrawable
    {
        public void draw(DirectxDrawer drawer)
        {
            if (isEnabled() && Context.GetInstance.Player.isAlive())
            {
                var _buffer = Utils.Clone.CloneObject<List<Model.EnemyPlayer>>(Context.GetInstance.Players);

                foreach (Model.EnemyPlayer player in _buffer)
                {
                    if (player.isValid() && player.isAlive() && player.Team != Context.GetInstance.Player.Team)
                    {
                        Color color = Color.Red;
                        if (player.isVisible())
                        {
                            color = Color.Green;
                        }

                        if (player.inVehicle() && player.Vehicle.IsDriver && EspUtils.shouldDrawOnVehicle())
                        {
                            if(player.Vehicle.AABBScreen != default(Model.VehicleAABBScreen))
                            {
                                var y = 9999;
                                var x = 9999;
                                var xx = 0;
                                var diff = 0;
                                FieldInfo[] fields = player.Vehicle.AABBScreen.GetType().GetFields();
                                foreach (var field in fields)
                                {
                                    Vector3 vector = (Vector3)field.GetValue(player.Vehicle.AABBScreen);
                                    if(vector.Y < y)
                                        y = (int)vector.Y;
                                    if (vector.X < x)
                                        x = (int)vector.X;
                                    if (vector.X > xx)
                                        xx = (int)vector.X;
                                }
                                if(xx > 0)
                                {
                                    diff = xx - x;
                                    drawer.DrawHealthBar(x, (int)y - 5, diff, 3, (int)player.Vehicle.Health, (int)player.Vehicle.MaxHealth);
                                }
                            }
                        }
                        else if(EspUtils.shouldDrawOnPlayer())
                        {
                            Vector3 Foot = Game.Engine.Math.Conversion.WorldToScreen(player.Position);
                            Vector3 Head;
                            Game.Engine.Math.Conversion.WorldToScreen(player.Position, player.Pose, out Head);

                            float HeadToFoot = Foot.Y - Head.Y;
                            float BoxWidth = HeadToFoot / 2;
                            float rectangleX = Head.X - ((BoxWidth) / 2);
                            drawer.DrawHealthBar((int)rectangleX, (int)Head.Y - 5, (int)BoxWidth, 3, (int)player.Health, (int)player.MaxHealth);
                        }
                    }
                }
            }
        }

        public bool isEnabled()
        {
            MenuFeature.BooleanMenuItem Item = (MenuFeature.BooleanMenuItem)MenuFeature.Helper.GetInstance.Items["EspHealth"];
            return Item.enabled;
        }

        public void tick()
        {
            return;
        }
    }
}
