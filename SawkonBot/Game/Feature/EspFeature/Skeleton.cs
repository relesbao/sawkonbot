﻿using SawkonBot.View.Helper;
using SharpDX;
using System.Collections.Generic;

namespace SawkonBot.Game.Feature.EspFeature
{
    class Skeleton : IFeature, IDrawable
    {
        public void draw(DirectxDrawer drawer)
        {
           
            var _buffer = Utils.Clone.CloneObject<List<Model.EnemyPlayer>>(Context.GetInstance.Players);
            foreach (Model.EnemyPlayer player in _buffer)
            {
                if (isEnabled() && Context.GetInstance.Player.isAlive())
                {
                    if (player.isValid() && player.isAlive() && player.Team != Context.GetInstance.Player.Team)
                    {
                        Color color = Color.Red;
                        if (player.isVisible())
                        {
                            color = Color.Green;
                        }
                        ForceUpdateOnBones(player);
                        drawer.DrawBone(player.Bone, color);
                    }
                }
             }            
        }

        public bool isEnabled()
        {
            MenuFeature.BooleanMenuItem Item = (MenuFeature.BooleanMenuItem)MenuFeature.Helper.GetInstance.Items["EspSkeleton"];
            return Item.enabled;
        }

        public void tick()
        {
            return;
        }

        public static void ForceUpdateOnBones(Model.EnemyPlayer player)
        {
            if (player.isValid())
            {
                byte occlusionFlags = Utils.Memory.Read<byte>(player.Soldier + 0x1A);
                if (!player.isVisible() && !player.inVehicle())
                {
                    if (occlusionFlags == 175)
                    {
                        byte flag = 143;
                        Utils.Memory.Write<byte>(player.Soldier + 0x1A, flag);
                    }
                }
            }            
        }
    }
}
