﻿using System;
using SawkonBot.View.Helper;
using SharpDX;
using System.Collections.Generic;

namespace SawkonBot.Game.Feature.EspFeature
{
    class Box : IFeature, IDrawable
    {
        public void draw(DirectxDrawer drawer)
        {
            if (isEnabled() && Context.GetInstance.Player.isAlive())
            {
                var _buffer = Utils.Clone.CloneObject<List<Model.EnemyPlayer>>(Context.GetInstance.Players);

                foreach (Model.Player player in _buffer)
                {
                    if (player.isValid() && player.isAlive() && player.Team != Context.GetInstance.Player.Team)
                    {
                        Color color = Color.Red;
                        if (player.isVisible())
                        {
                            color = Color.Green;
                        }
                        if (EspUtils.shouldDrawOnPlayer())
                        {
                            drawOnPlayer(player, color, drawer);
                        }
                        if(EspUtils.shouldDrawOnVehicle() && player.inVehicle() && player.Vehicle.IsDriver)
                        {
                            color = Color.OrangeRed;
                            drawOnVehicle(player, color, drawer);
                        }
                    }
                }
            }            
        }

        private void drawOnPlayer(Model.Player player, Color color, DirectxDrawer drawer)
        {
            if (getRenderingType() == "2D")
            {
                Vector3 Foot = Game.Engine.Math.Conversion.WorldToScreen(player.Position);
                Vector3 Head;
                Game.Engine.Math.Conversion.WorldToScreen(player.Position, player.Pose, out Head);

                float HeadToFoot = Foot.Y - Head.Y;
                float BoxWidth = HeadToFoot / 2;
                float X = Head.X - BoxWidth / 2;
                drawer.DrawRectangle((int)X, (int)Head.Y, (int)BoxWidth, (int)HeadToFoot, color);
            }
            else if (getRenderingType() == "3D")
            {
                drawer.DrawAABB(Engine.Math.Conversion.GetAABB(player), player.Position, player.Yaw, color);
            }
        }

        private void drawOnVehicle(Model.Player player, Color color, DirectxDrawer drawer)
        {
            drawer.DrawAABB(player.Vehicle.AABBScreen, color);
        }

        private string getRenderingType()
        {
            MenuFeature.ArrayMenuItem Item = (MenuFeature.ArrayMenuItem)MenuFeature.Helper.GetInstance.Items["EspBoxType"];
            return Item.options[Item.selected];
        }

        public bool isEnabled()
        {
            MenuFeature.BooleanMenuItem Item = (MenuFeature.BooleanMenuItem)MenuFeature.Helper.GetInstance.Items["EspBox"];
            return Item.enabled;
        }

        public void tick()
        {
            return;
        }
    }
}
