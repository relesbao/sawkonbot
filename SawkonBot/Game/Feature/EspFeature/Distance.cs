﻿using System;
using SawkonBot.View.Helper;
using SharpDX;
using System.Collections.Generic;

namespace SawkonBot.Game.Feature.EspFeature
{
    class Distance : IFeature, IDrawable
    {
        public void draw(DirectxDrawer drawer)
        {
            if (isEnabled() && Context.GetInstance.Player.isAlive())
            {
                var _buffer = Utils.Clone.CloneObject<List<Model.EnemyPlayer>>(Context.GetInstance.Players);

                foreach (Model.EnemyPlayer player in _buffer)
                {
                    if (player.isValid() && player.isAlive() && player.Team != Context.GetInstance.Player.Team)
                    {
                        Color color = Color.Red;
                        if (player.isVisible())
                        {
                            color = Color.Green;
                        }

                        if ((player.inVehicle() && EspUtils.shouldDrawOnVehicle() && player.Vehicle.IsDriver) || EspUtils.shouldDrawOnPlayer())
                        {
                            Vector3 Foot = Game.Engine.Math.Conversion.WorldToScreen(player.Position);
                            Vector3 Head;
                            Game.Engine.Math.Conversion.WorldToScreen(player.Position, player.Pose, out Head);

                            float HeadToFoot = Foot.Y - Head.Y;
                            float BoxWidth = HeadToFoot / 2;
                            float rectangleX = Head.X - ((BoxWidth) / 2);

                            drawer.DrawText(player.Distance.ToString("0") + "m", (int)rectangleX, (int)(Head.Y + HeadToFoot), 200, 20, false, color, Color.Black);
                        }
                    }
                }
            }
        }

        public bool isEnabled()
        {
            MenuFeature.BooleanMenuItem Item = (MenuFeature.BooleanMenuItem)MenuFeature.Helper.GetInstance.Items["EspDistance"];
            return Item.enabled;
        }

        public void tick()
        {
            return;
        }
    }
}
