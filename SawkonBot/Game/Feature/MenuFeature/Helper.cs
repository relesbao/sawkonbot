﻿using System;
using System.Collections.Generic;

namespace SawkonBot.Game.Feature.MenuFeature
{

    struct BooleanMenuItem
    {
        public string label;
        public bool enabled;

        public override String ToString()
        {
            return String.Format(label + " {0}", (enabled ? "[X]" : "[ ]"));
        }
    }

    struct ArrayMenuItem
    {
        public string label;
        public int selected;
        public string[] options;

        public override String ToString()
        {
            return String.Format(label + " [{0}]", options[selected]);
        }
    }

    class Helper
    {
        public IDictionary<string, ValueType> Items = new Dictionary<string, ValueType>();

        // Singleton
        private static Helper instance;
        private Helper() {

            ArrayMenuItem espDrawEntities;
            espDrawEntities.label = "DRAW ON";
            espDrawEntities.options = new string[] { "All", "Players", "Vehicles" };
            espDrawEntities.selected = 0;
            Items.Add("EspDrawEntities", espDrawEntities);

            BooleanMenuItem espBox;
            espBox.label = "ESP BOX";
            espBox.enabled = true;
            Items.Add("EspBox", espBox);

            ArrayMenuItem espBoxType;
            espBoxType.label = "ESP BOX TYPE";
            espBoxType.options = new string[] { "2D", "3D" };
            espBoxType.selected = 0;
            Items.Add("EspBoxType", espBoxType);            

            BooleanMenuItem espDistance;
            espDistance.label = "ESP DISTANCE";
            espDistance.enabled = false;
            Items.Add("EspDistance", espDistance);

            BooleanMenuItem espHealth;
            espHealth.label = "ESP HEALTH";
            espHealth.enabled = false;
            Items.Add("EspHealth", espHealth);

            BooleanMenuItem espLines;
            espLines.label = "ESP LINES";
            espLines.enabled = false;
            Items.Add("EspLines", espLines);

            BooleanMenuItem espSkeleton;
            espSkeleton.label = "PLAYER SKELETON";
            espSkeleton.enabled = false;
            Items.Add("EspSkeleton", espSkeleton);

            BooleanMenuItem noRecoil;
            noRecoil.label = "NO RECOIL";
            noRecoil.enabled = false;
            Items.Add("NoRecoil", noRecoil);

            BooleanMenuItem noSpread;
            noSpread.label = "NO SPREAD";
            noSpread.enabled = false;
            Items.Add("NoSpread", noSpread);

            BooleanMenuItem noBreath;
            noBreath.label = "NO BREATH";
            noBreath.enabled = false;
            Items.Add("NoBreath", noBreath);

            BooleanMenuItem aimbot;
            aimbot.label = "INFANTRY AIMBOT";
            aimbot.enabled = false;
            Items.Add("Aimbot", aimbot);
        }

        public static Helper GetInstance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Helper();
                }
                return instance;
            }
        }

        public void Toggle(string key)
        {
            if (Items[key] is ArrayMenuItem)
            {
                ArrayMenuItem Item = (ArrayMenuItem)Items[key];
                Item.selected++;
                if(Item.selected == Item.options.Length)
                {
                    Item.selected = 0;
                }

                Items[key] = Item;
            }

            if (Items[key] is BooleanMenuItem)
            {
                BooleanMenuItem Item = (BooleanMenuItem)Items[key];
                Item.enabled = !Item.enabled;
                Items[key] = Item;
            }            
        }
    }
}
