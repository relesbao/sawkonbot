﻿using SharpDX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SawkonBot.Game.Feature
{
    class Aimbot : IFeature, IKeysWatcher
    {
        private volatile bool _keyDown;
        public static AimbotFeature.Target _currentTarget;

        public static Int64 targetOffset;

        public bool isEnabled()
        {
            return true;
            MenuFeature.BooleanMenuItem Item = (MenuFeature.BooleanMenuItem)MenuFeature.Helper.GetInstance.Items["Aimbot"];
            return Item.enabled;
        }

        public void tick()
        {
            if (isEnabled() && _keyDown)
            {
                _currentTarget = AimbotFeature.TargetAcquirer.AcquireTarget();
                if (_currentTarget.Valid)
                {
                    Vector3 predictedPosition = AimbotFeature.Prediction.PredictAimingPos(_currentTarget.Bone, _currentTarget.Player.Velocity, Game.Context.GetInstance.Player.BulletSpeed, Game.Context.GetInstance.Player.BulletGravity);
                    try
                    {                        
                        var angles = Utils.Memory.Read<Int64>(Game.Engine.Angles.offset);
                        if (Utils.Memory.IsValid(angles))
                        {
                            var clientSoldierAimingSimulation = Utils.Memory.Read<Int64>(angles + Game.Engine.Angles.ClientSoldierAimingSimulationOffset);
                            if (Utils.Memory.IsValid(clientSoldierAimingSimulation))
                            {
                                var fpsAimer = Utils.Memory.Read<Int64>(clientSoldierAimingSimulation + Game.Engine.ClientSoldierAimingSimulation.fpsAimerOffset);
                                if (Utils.Memory.IsValid(fpsAimer))
                                {
                                    Vector2 sway = Utils.Memory.Read<Vector2>(clientSoldierAimingSimulation + 0x0028);
                                    var targetAngle = _getViewAngle(predictedPosition, sway);
                                    if (targetAngle.IsZero)
                                        return;

                                    var actualAngle = Utils.Memory.Read<Vector2>(fpsAimer + 0x14);

                                    // Smoothing
                                    if (targetOffset != _currentTarget.Player.Offset)
                                    {
                                        targetOffset = _currentTarget.Player.Offset;
                                        AimbotFeature.Smoothing.currentAimingTime = Vector2.Zero;
                                    }
                                    targetAngle = AimbotFeature.Smoothing.SmoothAngles(actualAngle, targetAngle);

                                    // Write angle
                                    Utils.Memory.Write<Vector2>(fpsAimer + 0x14, targetAngle); // X
                                }
                            }
                        }
                        
                    }
                    catch{
                        Console.WriteLine("Error");
                    }
                }
            }
        }

        public void watchKeys()
        {
            if (Utils.Keys.IsKeyDown(Utils.KeyCodes.RButton))
            {
                _keyDown = true;
            }
            else
            {
                _keyDown = false;
            }
        }

        private Vector2 _getViewAngle(Vector3 position, Vector2 sway)
        {
            Vector2 viewAngle;

            Vector3 calc = Vector3.Zero;
            calc.X = position.X - Game.Context.GetInstance.EngineContext.MatrixInverse.M41;
            calc.Y = position.Y - Game.Context.GetInstance.EngineContext.MatrixInverse.M42;
            calc.Z = position.Z - Game.Context.GetInstance.EngineContext.MatrixInverse.M43;
            calc.Normalize();

            viewAngle.X = (float)(-Math.Atan2((double)calc.X, (double)calc.Z));
            viewAngle.Y = (float)Math.Atan2((double)calc.Y, Math.Sqrt((double)(calc.X * calc.X + calc.Z * calc.Z)));

            viewAngle.X = viewAngle.X - sway.X;
            viewAngle.Y = viewAngle.Y - sway.Y;
            return viewAngle;
        }
    }
}
