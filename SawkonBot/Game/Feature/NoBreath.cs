﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SawkonBot.Game.Feature
{
    class NoBreath : IFeature, IKeysWatcher
    {
        public bool isEnabled()
        {
            MenuFeature.BooleanMenuItem Item = (MenuFeature.BooleanMenuItem)MenuFeature.Helper.GetInstance.Items["NoBreath"];
            return Item.enabled;
        }

        public void tick()
        {
            return;
        }

        public void watchKeys()
        {
            if (isEnabled() && Utils.Keys.IsKeyDown(Utils.KeyCodes.RButton))
            {
                Model.LocalPlayer player = Context.GetInstance.Player;
                if (player.ActiveSlot == 0 && !player.inVehicle() && !player.inVehicle())
                {
                    Int64 breathControl = Utils.Memory.Read<Int64>(player.Soldier + Game.Engine.Soldier.breathControlComponentOffset);
                    if (Utils.Memory.IsValid(breathControl))
                    {
                        float breathControlInput = Utils.Memory.Read<float>(breathControl + Game.Engine.BreathControl.BreathControlInputOffset);
                        if(breathControlInput > 0)
                        {
                            Utils.Memory.Write<float>(breathControl + Game.Engine.BreathControl.BreathControlInputOffset, 0);
                        }
                    }
                }
            }
        }
    }
}
