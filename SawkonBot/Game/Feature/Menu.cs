﻿using System;
using System.Linq;
using System.Windows.Forms;
using SawkonBot.View.Helper;
using SharpDX;


namespace SawkonBot.Game.Feature
{
    class Menu : IFeature, IDrawable, IKeysWatcher
    {
        private bool _enabled = false;

        private int _currentIndex = 0;

        public void draw(DirectxDrawer drawer)
        {
            if (isEnabled())
            {
                MenuFeature.Helper helper = MenuFeature.Helper.GetInstance;
                int y, originalY;
                y = originalY = 60;
                int x = 30;
                drawer.DrawText("SawkonBot Menu", x, y, 200, 30, false, Color.DarkGreen, Color.LightCyan);
                drawer.DrawLine(x - 5, 20 + y, 185 + x, 20 + y, Color.DarkCyan, 1.3f);
                y = y + 30;
                var i = 0;
                foreach (string key in helper.Items.Keys)
                {
                    Color color = Color.DarkCyan;
                    Color shadow = Color.DarkBlue;
                    if (i == _currentIndex)
                    {
                        color = Color.LightBlue;
                    }
                    drawer.DrawText(helper.Items[key].ToString(), x, y, 200, 22, false, color, shadow);
                    y = y + 22;
                    i++;
                }
                drawer.DrawLine(x - 10, (_currentIndex + 1) * 22 + 30 + originalY, 190 + x, (_currentIndex + 1) * 22 + 30 + originalY, Color.LightBlue, 1.3f);
                drawer.DrawBox(x - 10, originalY -15, 200, (y + 20) - originalY, new Color(0, 0, 0, 0.4f));
            }            
        }

        public bool isEnabled()
        {
            return _enabled;
        }

        public void watchKeys()
        {
            MenuFeature.Helper helper = MenuFeature.Helper.GetInstance;
            if (Utils.Keys.IsKeyDown(Utils.KeyCodes.Home))
            {
                _currentIndex = 0;
                _enabled = !_enabled;
            }

            if (_enabled)
            {
                if (Utils.Keys.IsKeyDown(Utils.KeyCodes.Down))
                {
                    _currentIndex++;
                    if (_currentIndex >= helper.Items.Count)
                    {
                        _currentIndex = 0;
                    }
                }

                if (Utils.Keys.IsKeyDown(Utils.KeyCodes.Up))
                {
                    _currentIndex--;
                    if (_currentIndex < 0)
                    {
                        _currentIndex = helper.Items.Count - 1;
                    }
                }

                if (Utils.Keys.IsKeyDown(Utils.KeyCodes.Right))
                {
                    string key = helper.Items.Keys.ElementAt(_currentIndex);
                    helper.Toggle(key);
                }
            }
            
        }

        public void tick()
        {
            return;
        }
    }
}
