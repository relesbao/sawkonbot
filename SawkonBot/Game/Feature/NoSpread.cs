﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SawkonBot.Game.Feature
{
    class NoSpread : IFeature
    {
        public bool isEnabled()
        {
            MenuFeature.BooleanMenuItem Item = (MenuFeature.BooleanMenuItem)MenuFeature.Helper.GetInstance.Items["NoSpread"];
            return Item.enabled;
        }

        public void tick()
        {
            if (isEnabled())
            {
                Model.LocalPlayer player = Context.GetInstance.Player;
                if (player.ActiveSlot == 0 && Utils.Memory.IsValid(player.GunSwayData))
                {
                    float DeviationScaleFactorZoom = Utils.Memory.Read<float>(player.GunSwayData + Engine.GunSwayData.deviationScaleFactorZoomOffset);

                    if (DeviationScaleFactorZoom != 0.0f)
                    {
                        Utils.Memory.Write<float>(player.GunSwayData + Engine.GunSwayData.deviationScaleFactorZoomOffset, 0.0f);
                        Utils.Memory.Write<float>(player.GunSwayData + Engine.GunSwayData.deviationScaleFactorNoZoomOffset, 0.0f);
                        Utils.Memory.Write<float>(player.GunSwayData + Engine.GunSwayData.gameplayDeviationScaleFactorZoomOffset, 0.0f);
                        Utils.Memory.Write<float>(player.GunSwayData + Engine.GunSwayData.gameplayDeviationScaleFactorNoZoomOffset, 0.0f);
                    }
                }
            }            
        }
    }
}
