﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SawkonBot.Game.Feature
{
    interface IDrawable
    {
        void draw(View.Helper.DirectxDrawer drawer);
    }
}
