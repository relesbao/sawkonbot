﻿using SharpDX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SawkonBot.Game.Feature.AimbotFeature
{
    static class Smoothing
    {

        public static Vector2 currentAimingTime = Vector2.Zero;
        private static Vector2 maxAimingTime = new Vector2(.7f, .7f);
        private static float deltaTime = 0.033f;

        public static Vector2 SmoothAngles(Vector2 from, Vector2 to)
        {
            currentAimingTime = currentAimingTime + new Vector2(deltaTime, deltaTime);
            if (currentAimingTime.X > maxAimingTime.X)
                currentAimingTime.X = maxAimingTime.X;
            if (currentAimingTime.Y > maxAimingTime.Y)
                currentAimingTime.Y = maxAimingTime.Y;

            Vector2 AimPercent = currentAimingTime / maxAimingTime;
            return LerpAngle(from, to, AimPercent.X);
        }

        public static Vector2 CerpAngle(Vector2 From, Vector2 To, float StepX, float StepY)
        {
            float CubicStepX = (float)(1 - Math.Cos(StepX * Math.PI)) / 2;
            float CubicStepY = (float)(1 - Math.Cos(StepY * Math.PI)) / 2;
            Vector2 Delta = (To - From);


            Utils.Math.MathUtils.NormalizeAngle(Delta);
            To.X = (From.X + CubicStepX * Delta.X);
            To.Y = (From.Y + CubicStepY * Delta.Y);
            Utils.Math.MathUtils.NormalizeAngle(To);
            return To;
        }

        public static Vector2 LerpAngle(Vector2 From, Vector2 To, float Step)
        {
            Console.WriteLine(Step);
            Vector2 Delta = (To - From);
            Utils.Math.MathUtils.NormalizeAngle(Delta);
            To = (From + Step * Delta);
            Utils.Math.MathUtils.NormalizeAngle(To);
            return To;
        }
    }
}
