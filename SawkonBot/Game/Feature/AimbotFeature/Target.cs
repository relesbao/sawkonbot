﻿using SharpDX;
using System.Collections.Generic;

namespace SawkonBot.Game.Feature.AimbotFeature
{
    struct Target
    {
        public bool Valid;
        public Model.EnemyPlayer Player;
        public float Distance;
        public Vector3 Bone;

        public static Target Initialize()
        {
            Target target;
            target.Valid = false;
            target.Player = null;
            target.Distance = 9999;
            target.Bone = Vector3.Zero;
            return target;
        }
    }

    class TargetAcquirer
    {
        public static Target AcquireTarget()
        {
            Target _target = Target.Initialize();

            float closest = 99999.0f;
            if (Context.GetInstance.Player.isValid() && Context.GetInstance.Player.isAlive())
            {
                var _buffer = Utils.Clone.CloneObject<List<Model.EnemyPlayer>>(Context.GetInstance.Players);
                foreach (Model.EnemyPlayer player in _buffer)
                {
                    if (player.CanBeTargeted())
                    {
                        EspFeature.Skeleton.ForceUpdateOnBones(player);
                        Vector3 bone = player.Bone.GetBoneByName(Config.GetTargetBoneName());
                        if (bone.IsZero)
                        {
                            return _target;
                        }
                        Vector3 boneToScreen = Game.Engine.Math.Conversion.WorldToScreen(bone);
                        Vector2 crosshair = new Vector2(Context.GetInstance.EngineContext.CrossX, Context.GetInstance.EngineContext.CrossY);
                        float distance = Utils.Math.MathUtils.CalculateDistance(new Vector2(boneToScreen.X, boneToScreen.Y), crosshair);
                        if(distance < closest)
                        {
                            closest = distance;
                            _target.Bone = bone;
                            _target.Distance = distance;
                            _target.Player = player;
                            _target.Valid = true;
                        }
                    }
                }
            } 
                   
            return _target;
        }
    }
}
