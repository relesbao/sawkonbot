﻿using SharpDX;
namespace SawkonBot.Game.Feature.AimbotFeature
{
    public static class Prediction
    {
        public static Vector3 PredictAimingPos(Vector3 targetPosition, Vector3 targetVelocity, float bulletVelocity, float bulletGravity)
        {
            Vector3 predictedAimingPosition = targetPosition;

            // trans target position relate to local player's view position for simplifying equations
            Vector3 p1 = targetPosition;

            p1.X -= Game.Context.GetInstance.EngineContext.MatrixInverse.M41;
            p1.Y -= Game.Context.GetInstance.EngineContext.MatrixInverse.M42;
            p1.Z -= Game.Context.GetInstance.EngineContext.MatrixInverse.M43;

            // equations about predict time t
            //
            // unknowns
            // t: predict hit time
            // p: predict hit position at time 't'
            // v0: predict local player's bullet velocity vector which could hit target at at time 't' and position 'p'
            //
            // knowns
            // p1: target player's current position relate to local player's view position
            // v1: target player's velocity
            // |v0|: local player's bullet velocity
            // g: local player's bullet gravity
            //
            // =>
            //
            // vx0^2 + vy0^2 + vz0^2 = |v0|^2
            //
            // px = vx0*t
            // py = vy0*t + 0.5*g*t^2
            // pz = vz0*t
            //
            // px = px1 + vx1*t
            // py = py1 + vy1*t
            // pz = pz1 + vz1*t
            //
            // cause all positions are relate to local player's view position, so there is no p0 in above equations
            //
            // =>
            //
            // vx0 = px1/t + vx1
            // vy0 = py1/t + vy1 - 0.5g*t
            // vz0 = pz1/t + vz1
            //
            // with above three equations and the first equation about v0,  we got the final quartic equation about predict time 't'
            // (0.25*g^2)*(t^4) + (-g*vy1)*(t^3) + (vx1^2+vy1^2+vz1^2 - g*py1 - |v|^2)*(t^2) + 2*(px1*vx1+py1*vy1+pz1*vz1)*(t) + (px1^2+py1^2+pz1^2) = 0
            //
            // let's solve this problem...
            //
            double a = bulletGravity * bulletGravity * 0.25;
            double b = -bulletGravity * targetVelocity.Y;
            double c = targetVelocity.X * targetVelocity.X + targetVelocity.Y * targetVelocity.Y + targetVelocity.Z * targetVelocity.Z - bulletGravity * p1.Y - bulletVelocity * bulletVelocity;
            double d = 2.0 * (p1.X * targetVelocity.X + p1.Y * targetVelocity.Y + p1.Z * targetVelocity.Z);
            double e = p1.X * p1.X + p1.Y * p1.Y + p1.Z * p1.Z;

            // some unix guys will not afraid these two lines
            double[] roots = new double[4];
            uint num_roots = Utils.Math.MathUtils.SolveQuartic(a, b, c, d, e, ref roots);

            if (num_roots > 0)
            {
                // find the best predict hit time
                // smallest 't' for guns, largest 't' for something like mortar with beautiful arcs
                double hitTime = 0.0;
                for (int i = 0; i < num_roots; ++i)
                {
                    if (roots[i] > 0.0 && (hitTime == 0.0 || roots[i] < hitTime))
                        hitTime = roots[i];
                }

                if (hitTime > 0.0)
                {
                    // get predict bullet velocity vector at aiming direction
                    double hitVelX = p1.X / hitTime + targetVelocity.X;
                    double hitVelY = p1.Y / hitTime + targetVelocity.Y - 0.5 * bulletGravity * hitTime;
                    double hitVelZ = p1.Z / hitTime + targetVelocity.Z;

                    // finally, the predict aiming position in world space
                    predictedAimingPosition.X = (float)(Game.Context.GetInstance.EngineContext.MatrixInverse.M41 + hitVelX * hitTime);
                    predictedAimingPosition.Y = (float)(Game.Context.GetInstance.EngineContext.MatrixInverse.M42 + hitVelY * hitTime);
                    predictedAimingPosition.Z = (float)(Game.Context.GetInstance.EngineContext.MatrixInverse.M43 + hitVelZ * hitTime);
                }
            }

            return predictedAimingPosition;
        }
    }
}

