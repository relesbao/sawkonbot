﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SawkonBot.Game.Feature
{
    class NoRecoil : IFeature
    {
        public bool isEnabled()
        {
            MenuFeature.BooleanMenuItem Item = (MenuFeature.BooleanMenuItem)MenuFeature.Helper.GetInstance.Items["NoRecoil"];
            return Item.enabled;
        }

        public void tick()
        {
            if (isEnabled())
            {
                Model.LocalPlayer player = Context.GetInstance.Player;
                if (player.ActiveSlot == 0 && !player.inVehicle() && Utils.Memory.IsValid(player.GunSwayData))
                {
                    float FirstShotRecoilMultiplier = Utils.Memory.Read<float>(player.GunSwayData + Engine.GunSwayData.firstShotRecoilMultiplierOffset);

                    if (FirstShotRecoilMultiplier != 0.0f)
                    {
                        Utils.Memory.Write<float>(player.GunSwayData + Engine.GunSwayData.shootingRecoilDecreaseScaleOffset, 100.0f);
                        Utils.Memory.Write<float>(player.GunSwayData + Engine.GunSwayData.firstShotRecoilMultiplierOffset, 0.0f);
                    }
                }
            }            
        }
    }
}
