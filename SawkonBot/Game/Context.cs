﻿using SawkonBot.Utils;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace SawkonBot.Game
{
    class Context
    {
        private Model.LocalPlayer _player;
        private List<Model.EnemyPlayer> _players = null;
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public Model.LocalPlayer Player
        {
            get
            {
                return _player;
            }
        }

        public List<Model.EnemyPlayer> Players
        {
            get
            {
                return _players;
            }
        }

        private Engine.Context engineContext;
        public Engine.Context EngineContext
        {
            get
            {
                return this.engineContext;
            }
        }

        private ThreadRunner RefreshTimer;

        // Singleton
        private static Context instance;
        private Context()
        {
            engineContext = new Engine.Context();
            _players = new List<Model.EnemyPlayer>();
            RefreshTimer = new ThreadRunner(100);
            RefreshTimer.TickEvent += this.refresh;
            RefreshTimer.Start();
        }
        public static Context GetInstance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Context();
                }
                return instance;
            }
        }

        public void refresh(object sender, EventArgs e)
        {
            EngineContext.refresh();
            List<Model.EnemyPlayer> buffer = new List<Model.EnemyPlayer>();
            _player = new Model.LocalPlayer(engineContext.GetLocalPlayerOffset());
            if (_player.isValid())
            {
                _player.LoadFromMemory();
                for (uint i = 0; i < engineContext.GetMaxPlayers(); i++)
                {
                    Int64 playerOffset = engineContext.GetPlayerOffset(i);
                    if (Memory.IsValid(playerOffset) && playerOffset != _player.Offset)
                    {
                        Model.EnemyPlayer enemyPlayer;
                        enemyPlayer = new Model.EnemyPlayer(playerOffset);
                        if (enemyPlayer.isValid() && enemyPlayer.isEnemy())
                        {
                            enemyPlayer.LoadFromMemory();
                            buffer.Add(enemyPlayer);
                        }
                    }
                }
            }            
            _players.Clear();
            _players = buffer;
        }
    }
}
