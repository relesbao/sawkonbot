﻿using System;

namespace SawkonBot.Game.Model
{
    class EnemyPlayer : Player
    {
        public float Distance { get; private set; }
        public Engine.ClientPlayer.Bone Bone { get; private set; }

        public EnemyPlayer(Int64 Offset) : base(Offset)
        {
            
        }

        public new void LoadFromMemory()
        {
            base.LoadFromMemory();
            if (isValid())
            {
                Distance = (float)Utils.Math.MathUtils.CalculateDistance(Context.GetInstance.Player.Position, this.Position);
                Bone = Engine.ClientPlayer.Resolver.GetBone(Soldier);
            }
        }

        public bool CanBeTargeted()
        {
            return (isValid() && isAlive() && isVisible() && !inVehicle());
        }
    }
}
