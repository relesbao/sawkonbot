﻿using System;

namespace SawkonBot.Game.Model
{
    class LocalPlayer : Player
    {
        public Int64 WeaponComponent { get; private set; }
        public Int64 WeaponHandler { get; private set; }
        public Int32 ActiveSlot { get; private set; }
        public Int64 SoldierWeapon { get; private set; }
        public Int64 WeaponFiring { get; private set; }
        public Int64 GunSwayData { get; private set; }

        public float BulletSpeed { get; private set; }
        public float BulletGravity { get; private set; }

        public LocalPlayer(Int64 Offset) : base(Offset)
        {            
        }

        public new void LoadFromMemory()
        {
            base.LoadFromMemory();
            WeaponComponent = Engine.ClientPlayer.Resolver.GetWeaponComponent(Soldier);
            WeaponHandler = Engine.ClientPlayer.Resolver.GetWeaponHandler(WeaponComponent);
            ActiveSlot = Engine.ClientPlayer.Resolver.GetActiveSlot(WeaponComponent);

            SoldierWeapon = Engine.ClientPlayer.Resolver.GetSoldierWeapon(WeaponHandler, ActiveSlot);
            WeaponFiring = Engine.ClientPlayer.Resolver.GetWeaponFiring(SoldierWeapon);
            GunSwayData = Engine.ClientPlayer.Resolver.GetGunSwayData(WeaponFiring);

            float bulletSpeed, bulletGravity;
            Engine.ClientPlayer.Resolver.GetBulletSpeedAndGravity(WeaponFiring, out bulletSpeed, out bulletGravity);
            BulletSpeed = bulletSpeed;
            BulletGravity = bulletGravity;
        }

        public new bool isEnemy()
        {
            return false;
        }
    }
}
