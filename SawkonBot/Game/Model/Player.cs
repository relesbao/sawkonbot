﻿using SawkonBot.Utils;
using SharpDX;
using System;

namespace SawkonBot.Game.Model
{
    class Player
    {
        public Int64 Offset;
        public Int64 Soldier;

        public string Name { get; private set; }
        public float Health { get; private set; }
        public float MaxHealth { get; private set; }
        public float Yaw { get; private set; }
        public Vector3 Position { get; private set; }
        public Vector3 Velocity { get; private set; }
        public int Team { get; private set; }
        public bool Occluded { get; private set; }
        public int Pose{ get; private set; }
        public bool Spectator { get; private set; }
        public Model.Vehicle Vehicle { get; private set; }

        public Player(Int64 offset)
        {
            Offset = offset;
            if (Memory.IsValid(Offset))
            {
                Team = Engine.ClientPlayer.Resolver.GetTeam(Offset);
                Spectator = Engine.ClientPlayer.Resolver.IsSpectator(Offset);
            }            
        }

        public void LoadFromMemory()
        {
            Name = Engine.ClientPlayer.Resolver.GetName(Offset);
            Vehicle = new Model.Vehicle(Offset, this);
            Soldier = Engine.ClientPlayer.Resolver.GetSoldier(Offset, Vehicle.Offset);
            Pose = Engine.ClientPlayer.Resolver.GetPose(Soldier);
            Occluded = Engine.ClientPlayer.Resolver.GetOccluded(Soldier);
            Position = Engine.ClientPlayer.Resolver.GetPosition(Soldier);
            Velocity = Engine.ClientPlayer.Resolver.GetVelocity(Soldier);
            Yaw = Engine.ClientPlayer.Resolver.GetYaw(Soldier);

            float health, maxhealth;
            Engine.ClientPlayer.Resolver.GetHealth(Soldier, out health, out maxhealth);
            Health = health;
            MaxHealth = maxhealth;
            // Load the vehicle data
            Vehicle.LoadFromMemory();
        }

        public bool isValid()
        {
            return Memory.IsValid(Offset);
        }

        public bool isAlive()
        {
            return Health > 0.1f && Health <= 100 && !Position.IsZero;
        }

        public bool isVisible()
        {
            return !Occluded;
        }

        public bool isEnemy()
        {
            if(Context.GetInstance.Player != null)
            {
                return this.Team != Context.GetInstance.Player.Team && !Spectator;
            }
            return false;
        }

        public bool inVehicle()
        {
            if(Vehicle != null)
            {
                return Vehicle.IsValid() && Vehicle.Health > 0;
            }
            return false;
        }
    }
}
