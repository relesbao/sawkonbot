﻿using System;
using SharpDX;
using Matrix4x4 = System.Numerics.Matrix4x4;
using System.Collections.Generic;

namespace SawkonBot.Game.Model
{
    class Vehicle
    {

        public Int64 Offset { get; private set; }
        public Model.Player Player { get; private set; }

        public Utils.Math.AxisAlignedBox AABB { get; private set; }
        public float Health { get; private set; }
        public float MaxHealth { get; private set; }
        public string Name { get; private set; }
        public Matrix4x4 Transform { get; private set; }
        public bool IsDriver { get; private set; }
        public VehicleAABBScreen AABBScreen { get; private set; }

        public Vehicle(Int64 offset, Model.Player player)
        {
            Player = player;
            Offset = Engine.ClientPlayer.Resolver.GetVehicle(offset);            
        }

        public void LoadFromMemory()
        {
            if (IsValid())
            {
                IsDriver = Engine.ClientPlayer.Resolver.IsDriver(Player.Offset);
                if (IsDriver)
                {
                    AABB = Engine.ClientPlayer.Resolver.GetVehicleAABB(Offset);
                    Transform = Engine.ClientPlayer.Resolver.GetVehicleTransform(Offset);
                    Int64 vehicleEntityData = Engine.ClientPlayer.Resolver.GetVehicleEntityData(Offset);
                    Name = Engine.ClientPlayer.Resolver.GetVehicleName(vehicleEntityData);
                    MaxHealth = Engine.ClientPlayer.Resolver.GetVehicleMaxHealth(vehicleEntityData);
                    Health = Engine.ClientPlayer.Resolver.GetVehicleHealth(Offset);
                    _calculateAABBToScreen();
                }
            }
        }

        public bool IsValid()
        {
            return Utils.Memory.IsValid(Offset);
        }

        private bool _calculateAABBToScreen()
        {
            Vector3 vector3 = new Vector3(Transform.M41, Transform.M42, Transform.M43);
            Vector3 _Screen1 = Utils.Math.MathUtils.MultiplyVector(new Vector3(AABB.Min.X, AABB.Min.Y, AABB.Min.Z), Transform) + vector3;
            Vector3 _Screen2 = Utils.Math.MathUtils.MultiplyVector(new Vector3(AABB.Max.X, AABB.Max.Y, AABB.Max.Z), Transform) + vector3;
            Vector3 _Screen3 = Utils.Math.MathUtils.MultiplyVector(new Vector3(AABB.Min.X, AABB.Min.Y, AABB.Max.Z), Transform) + vector3;
            Vector3 _Screen4 = Utils.Math.MathUtils.MultiplyVector(new Vector3(AABB.Max.X, AABB.Max.Y, AABB.Min.Z), Transform) + vector3;
            Vector3 _Screen5 = Utils.Math.MathUtils.MultiplyVector(new Vector3(AABB.Max.X, AABB.Min.Y, AABB.Min.Z), Transform) + vector3;
            Vector3 _Screen6 = Utils.Math.MathUtils.MultiplyVector(new Vector3(AABB.Max.X, AABB.Min.Y, AABB.Max.Z), Transform) + vector3;
            Vector3 _Screen7 = Utils.Math.MathUtils.MultiplyVector(new Vector3(AABB.Min.X, AABB.Max.Y, AABB.Max.Z), Transform) + vector3;
            Vector3 _Screen8 = Utils.Math.MathUtils.MultiplyVector(new Vector3(AABB.Min.X, AABB.Max.Y, AABB.Min.Z), Transform) + vector3;
            if (!Game.Engine.Math.Conversion.WorldToScreen(_Screen1, out _Screen1)
                || !Game.Engine.Math.Conversion.WorldToScreen(_Screen2, out _Screen2)
                || (!Game.Engine.Math.Conversion.WorldToScreen(_Screen3, out _Screen3)
                || !Game.Engine.Math.Conversion.WorldToScreen(_Screen4, out _Screen4))
                || (!Game.Engine.Math.Conversion.WorldToScreen(_Screen5, out _Screen5)
                || !Game.Engine.Math.Conversion.WorldToScreen(_Screen6, out _Screen6)
                || (!Game.Engine.Math.Conversion.WorldToScreen(_Screen7, out _Screen7)
                || !Game.Engine.Math.Conversion.WorldToScreen(_Screen8, out _Screen8))))
            {
                return false;
            }
            VehicleAABBScreen aabbscreen;
            aabbscreen._Screen1 = _Screen1;
            aabbscreen._Screen2 = _Screen2;
            aabbscreen._Screen3 = _Screen3;
            aabbscreen._Screen4 = _Screen4;
            aabbscreen._Screen5 = _Screen5;
            aabbscreen._Screen6 = _Screen6;
            aabbscreen._Screen7 = _Screen7;
            aabbscreen._Screen8 = _Screen8;
            AABBScreen = aabbscreen;
            return true;
        }
    }


    struct VehicleAABBScreen
    {
        public Vector3 _Screen1;
        public Vector3 _Screen2;
        public Vector3 _Screen3;
        public Vector3 _Screen4;
        public Vector3 _Screen5;
        public Vector3 _Screen6;
        public Vector3 _Screen7;
        public Vector3 _Screen8;

        public static bool operator ==(VehicleAABBScreen op1, VehicleAABBScreen op2)
        {
            return op1.Equals(op2);
        }

        public static bool operator !=(VehicleAABBScreen op1, VehicleAABBScreen op2)
        {
            return !op1.Equals(op2);
        }
    }
}
