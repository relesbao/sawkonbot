﻿using SharpDX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace SawkonBot.Game
{
    class Manager
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private List<Game.Feature.IFeature> _features;

        private Utils.ThreadRunner DrawThread;
        private Utils.ThreadRunner LogicThread;

        private Utils.ThreadRunner KeysWatcher;

        private View.Helper.DirectxDrawer _drawer;
        private View.Stage _window;

        // Singleton
        private static Manager instance;
        private Manager() {
            _features = new List<Feature.IFeature>();

            // Register features
            _features.Add(new Feature.End());
            _features.Add(new Feature.Menu());
            _features.Add(new Feature.EspFeature.Box());
            _features.Add(new Feature.EspFeature.Skeleton());
            _features.Add(new Feature.EspFeature.Distance());
            _features.Add(new Feature.EspFeature.Lines());
            _features.Add(new Feature.EspFeature.HealthBar());
            _features.Add(new Feature.NoRecoil());
            _features.Add(new Feature.NoSpread());
            _features.Add(new Feature.NoBreath());
            _features.Add(new Feature.Aimbot());

            // Start the timer
            DrawThread = new Utils.ThreadRunner(60);
            DrawThread.TickEvent += this.draw;
            DrawThread.Start();

            LogicThread = new Utils.ThreadRunner(100);
            LogicThread.TickEvent += this.logic;
            LogicThread.Start();

            KeysWatcher = new Utils.ThreadRunner(10);
            KeysWatcher.TickEvent += watchKeys;
            KeysWatcher.Start();
        }

        public static Manager GetInstance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Manager();
                }
                return instance;
            }
        }

        public void SetWindow(View.Stage stage)
        {
            _window = stage;
            _drawer = new View.Helper.DirectxDrawer(stage);
        }

        public void ResizeWindow(Size2 size)
        {
            if(_drawer != null)
            {
                _drawer.Resize(size);
            }
        }

        private void logic(object sender, EventArgs e)
        {
            try
            {
                foreach (Feature.IFeature feature in _features)
                {
                    feature.tick();
                }
            }
            catch { }
        }

        private void draw(object sender, EventArgs e)
        {
            try
            {
                if(_window != null)
                {
                    // Update the window
                    _window.Invoke((MethodInvoker)(() => { _window.Tick(); }));
                    if (_drawer != null)
                    {
                        _drawer.BeginDraw();
                        foreach (Feature.IFeature feature in _features)
                        {
                            if (feature is Feature.IDrawable)
                            {
                                Feature.IDrawable drawableFeature = (Feature.IDrawable)feature;
                                drawableFeature.draw(_drawer);
                            }
                        }
                        _drawer.EndDraw();
                    }                    
                }
                
            }catch { }
        }

        public void watchKeys(object sender, EventArgs e)
        {
            if (!Process.GetInstance.isRunning())
            {
                Environment.Exit(0);
            }

            foreach (Feature.IKeysWatcher feature in _features.OfType<Feature.IKeysWatcher>())
            {
                feature.watchKeys();
            }
        }

        public void Stop()
        {
            if (DrawThread != null)
            {
                DrawThread.Stop();
            }
            if (LogicThread != null)
            {
                LogicThread.Stop();
            }
            if (KeysWatcher != null)
            {
                KeysWatcher.Stop();
            }
        }
    }
}
