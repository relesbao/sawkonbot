﻿using System;
using System.Runtime.InteropServices;
using SystemProcess = System.Diagnostics.Process;

namespace SawkonBot.Game
{
    class Process
    {
        private SystemProcess _process;
        private IntPtr _handler;
        const string PROCESS_NAME = "bf4";

        public static uint PROCESS_VM_READ = 0x0010;
        public static uint PROCESS_VM_WRITE = 0x0020;
        public static uint PROCESS_VM_OPERATION = 0x0008;
        public static uint PAGE_READWRITE = 0x0004;

        [DllImport("kernel32.dll")]
        public static extern IntPtr OpenProcess(UInt32 dwAccess, bool inherit, int pid);

        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern bool VirtualProtectEx(IntPtr hProcess, IntPtr lpAddress, UInt32 dwSize, uint flNewProtect, out uint lpflOldProtect);

        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern bool ReadProcessMemory(IntPtr hProcess, Int64 lpBaseAddress, [In, Out] byte[] lpBuffer, UInt64 dwSize, out IntPtr lpNumberOfBytesRead);

        [DllImport("kernel32.dll")]
        public static extern bool WriteProcessMemory(IntPtr hProcess, Int64 lpBaseAddress, [In, Out] byte[] lpBuffer, UInt64 dwSize, out IntPtr lpNumberOfBytesWritten);

        // Singleton
        private static Process instance;
        private Process() { }
        public static Process GetInstance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Process();
                }
                return instance;
            }
        }

        public bool isRunning()
        {
            SystemProcess[] pList = SystemProcess.GetProcessesByName(Process.PROCESS_NAME);
            this._process = pList.Length > 0 ? pList[0] : null;
            return this._process != null;
        }

        public IntPtr GetWindowHandler()
        {
            if (!isRunning())
            {
                throw new Exception("Calling GetWindowHandler but the process is not running");
            }
            return _process.MainWindowHandle;
        }

        public IntPtr GetHandler()
        {
            if (_handler == IntPtr.Zero)
            {
                _handler = OpenProcess(PROCESS_VM_READ | PROCESS_VM_WRITE | PROCESS_VM_OPERATION, false, _process.Id);
            }
            return _handler;
        }
    }
}
