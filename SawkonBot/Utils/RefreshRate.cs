﻿using System;
using System.Windows.Forms;

namespace SawkonBot.Utils
{
    internal class RefreshRate : Timer
    {
        private int refreshRate = 60;

        public int Rate
        {
            get
            {
                return this.refreshRate;
            }
            set
            {
                base.Interval = 0x3E8 / value;
                this.refreshRate = value;
            }
        }

        public RefreshRate(EventHandler eventHandler)
        {
            base.Interval = 0x3E8 / this.refreshRate;
            base.Tick += eventHandler;
        }
    }
}
