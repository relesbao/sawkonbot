﻿using System;
using System.Threading;
using SystemThread = System.Threading.Thread;

namespace SawkonBot.Utils
{
    class ThreadRunner
    {
        private SystemThread _thread;
        private bool _running;

        public int Interval { get; set; }

        public event EventHandler<EventArgs> TickEvent;
        public virtual void OnTickEvent(EventArgs e)
        {
            TickEvent?.Invoke(this, e);
        }

        public ThreadRunner(int tickRate)
        {
            this.Interval = 1000 / tickRate;
        }
        public ThreadRunner() : this(60) { }


        public void Start()
        {
            if (_thread != null)
                Stop();
            _running = true;
            _thread = new SystemThread(new ThreadStart(Tick));
            _thread.IsBackground = true;
            _thread.Priority = ThreadPriority.Highest;
            _thread.Start();
        }

        public void Tick()
        {
            while (_running)
            {
                this.OnTickEvent(new EventArgs());
                SystemThread.Sleep(Interval);
            }
        }

        public void Stop()
        {
            _running = false;
            if (_thread == null)
                return;
            if (_thread.ThreadState == ThreadState.Running)
                _thread.Abort();
            _thread = null;
        }
    }
}
