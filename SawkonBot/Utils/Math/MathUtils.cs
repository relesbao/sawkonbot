﻿using SharpDX;
using Matrix4x4 = System.Numerics.Matrix4x4;
using System;

namespace SawkonBot.Utils.Math
{

    public class MathUtils
    {
        public static float CalculateDistance(Vector3 from, Vector3 to)
        {
            Vector3 Distance = new Vector3();

            Distance.X = to.X - from.X;
            Distance.Y = to.Y - from.Y;
            Distance.Z = to.Z - from.Z;

            return (float)System.Math.Sqrt((Distance.X * Distance.X) + (Distance.Y * Distance.Y) + (Distance.Z * Distance.Z));
        }

        public static float CalculateDistance(Vector2 from, Vector2 to)
        {
            Vector2 distanceFromCenter = from - to;
            return distanceFromCenter.Length();
        }

        public static Vector3 MultiplyVector(Vector3 vector, Matrix4x4 mat)
        {
            return new Vector3((float)((double)mat.M11 * (double)vector.X + (double)mat.M21 * (double)vector.Y + (double)mat.M31 * (double)vector.Z), (float)((double)mat.M12 * (double)vector.X + (double)mat.M22 * (double)vector.Y + (double)mat.M32 * (double)vector.Z), (float)((double)mat.M13 * (double)vector.X + (double)mat.M23 * (double)vector.Y + (double)mat.M33 * (double)vector.Z));
        }

        public static Vector2 NormalizeAngle(Vector2 Angle)
        {
            if (Angle.X <= -System.Math.PI)
                Angle.X += (float)(2 * System.Math.PI);
            if (Angle.X > System.Math.PI)
                Angle.X -= (float)(2 * System.Math.PI);
            if (Angle.Y <= -System.Math.PI / 2)
                Angle.Y += (float)(System.Math.PI);
            if (Angle.Y > System.Math.PI / 2)
                Angle.Y -= (float)(System.Math.PI);
            return Angle;
        }

        public static uint SolveQuartic(double a, double b, double c, double d, double e, ref double[] x)
        {
            /* Adjust coefficients */
            double a1 = d / e;
            double a2 = c / e;
            double a3 = b / e;
            double a4 = a / e;

            /* Reduce to solving cubic equation */
            double q = a2 - a1 * a1 * 3 / 8;
            double r = a3 - a1 * a2 / 2 + a1 * a1 * a1 / 8;
            double s = a4 - a1 * a3 / 4 + a1 * a1 * a2 / 16 - 3 * a1 * a1 * a1 * a1 / 256;

            double[] coeff_cubic = new double[4];
            double[] roots_cubic = new double[3];
            double positive_root = 0;

            coeff_cubic[3] = 1;
            coeff_cubic[2] = q / 2;
            coeff_cubic[1] = (q * q - 4 * s) / 16;
            coeff_cubic[0] = -r * r / 64;
            uint nRoots = SolveCubic(coeff_cubic, ref roots_cubic);
            for (int i = 0; i < nRoots; i++)
            {
                if (roots_cubic[i] > 0)
                {
                    positive_root = roots_cubic[i];
                }
            }
            /* Reduce to solving two quadratic equations */
            double k = System.Math.Sqrt(positive_root);
            double l = 2 * k * k + q / 2 - r / (4 * k);
            double m = 2 * k * k + q / 2 + r / (4 * k);
            nRoots = 0;
            if (k * k - l > 0)
            {
                x[nRoots + 0] = -k - System.Math.Sqrt(k * k - l) - a1 / 4;
                x[nRoots + 1] = -k + System.Math.Sqrt(k * k - l) - a1 / 4;

                nRoots += 2;
            }
            if (k * k - m > 0)
            {
                x[nRoots + 0] = +k - System.Math.Sqrt(k * k - m) - a1 / 4;
                x[nRoots + 1] = +k + System.Math.Sqrt(k * k - m) - a1 / 4;

                nRoots += 2;
            }
            return nRoots;
        }

        public static uint SolveCubic(double[] coeff, ref double[] x)
        {
            /* Adjust coefficients */
            double a1 = coeff[2] / coeff[3];
            double a2 = coeff[1] / coeff[3];
            double a3 = coeff[0] / coeff[3];

            double Q = (a1 * a1 - 3 * a2) / 9;
            double R = (2 * a1 * a1 * a1 - 9 * a1 * a2 + 27 * a3) / 54;
            double Qcubed = Q * Q * Q;
            double d = Qcubed - R * R;

            /* Three real roots */
            if (d >= 0)
            {
                double theta = System.Math.Acos(R / System.Math.Sqrt(Qcubed));
                double sqrtQ = System.Math.Sqrt(Q);

                x[0] = -2 * sqrtQ * System.Math.Cos(theta / 3) - a1 / 3;
                x[1] = -2 * sqrtQ * System.Math.Cos((theta + 2 * System.Math.PI) / 3) - a1 / 3;
                x[2] = -2 * sqrtQ * System.Math.Cos((theta + 4 * System.Math.PI) / 3) - a1 / 3;

                return (3);
            }

            /* One real root */
            else
            {
                double e = System.Math.Pow(System.Math.Sqrt(-d) + System.Math.Abs(R), 1.0 / 3.0);

                if (R > 0)
                {
                    e = -e;
                }

                x[0] = (e + Q / e) - a1 / 3.0;

                return (1);
            }
        }
    } 


    public struct AxisAlignedBox
    {
        public Vector4 Min;
        public Vector4 Max;
    }
}
