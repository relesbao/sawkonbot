﻿using System;

namespace SawkonBot.Utils
{
    class ConsoleSpinner
    {
        private char[] _frames;
        private int _currentFrame;

        public ConsoleSpinner()
        {
            this._frames = new[]
            {
                '|',
                '/',
                '-',
                '\\'
            };
        }

        public void Spin()
        {
            var originalLeft = Console.CursorLeft;
            var originalTop = Console.CursorTop;

            Console.Write(_frames[_currentFrame]);

            Console.SetCursorPosition(originalLeft, originalTop);

            _currentFrame++;
            if(_currentFrame == _frames.Length)
                _currentFrame = 0;
        }
    }
}
