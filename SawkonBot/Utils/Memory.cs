﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Text;
using Matrix4x4 = System.Numerics.Matrix4x4;

namespace SawkonBot.Utils
{
    class Memory
    {
        public static T Read<T>(Int64 address)
        {            
            byte[] Buffer = new byte[Marshal.SizeOf(typeof(T))];
            IntPtr ByteRead;
            Game.Process.ReadProcessMemory(Game.Process.GetInstance.GetHandler(), address, Buffer, (uint)Buffer.Length, out ByteRead);

            GCHandle handle = GCHandle.Alloc(Buffer, GCHandleType.Pinned);
            T data = (T)Marshal.PtrToStructure(handle.AddrOfPinnedObject(), typeof(T));
            handle.Free();
            return data;
        }

        public static bool Write<T>(Int64 address, T t)
        {
            Byte[] Buffer = new Byte[Marshal.SizeOf(typeof(T))];
            GCHandle handle = GCHandle.Alloc(t, GCHandleType.Pinned);
            Marshal.Copy(handle.AddrOfPinnedObject(), Buffer, 0, Buffer.Length);
            handle.Free();
            uint oldProtect;
            Game.Process.VirtualProtectEx(Game.Process.GetInstance.GetHandler(), (IntPtr)address, (uint)Buffer.Length, Game.Process.PAGE_READWRITE, out oldProtect);
            IntPtr ptrBytesWritten;
            return Game.Process.WriteProcessMemory(Game.Process.GetInstance.GetHandler(), address, Buffer, (uint)Buffer.Length, out ptrBytesWritten);
        }

        public static string ReadString(Int64 address, UInt64 _Size)
        {
            byte[] buffer = new byte[_Size];
            IntPtr BytesRead;

            Game.Process.ReadProcessMemory(Game.Process.GetInstance.GetHandler(), address, buffer, _Size, out BytesRead);
            return Encoding.ASCII.GetString(buffer);
        }

        public static long ReadInt64(Int64 _lpBase)
        {
            IntPtr intPtr;
            byte[] numArray = new byte[8];
            Game.Process.ReadProcessMemory(Game.Process.GetInstance.GetHandler(), _lpBase, numArray, (ulong)8, out intPtr);
            return BitConverter.ToInt64(numArray, 0);
        }

        public static Matrix4x4 ReadMatrix(long _lpBaseAddress)
        {
            IntPtr intPtr;
            Matrix4x4 gravity = new Matrix4x4();
            byte[] numArray = new byte[64];
            Game.Process.ReadProcessMemory(Game.Process.GetInstance.GetHandler(), _lpBaseAddress, numArray, (ulong)64, out intPtr);
            gravity.M11 = BitConverter.ToSingle(numArray, 0);
            gravity.M12 = BitConverter.ToSingle(numArray, 4);
            gravity.M13 = BitConverter.ToSingle(numArray, 8);
            gravity.M14 = BitConverter.ToSingle(numArray, 12);
            gravity.M21 = BitConverter.ToSingle(numArray, 16);
            gravity.M22 = BitConverter.ToSingle(numArray, 20);
            gravity.M23 = BitConverter.ToSingle(numArray, 24);
            gravity.M24 = BitConverter.ToSingle(numArray, 28);
            gravity.M31 = BitConverter.ToSingle(numArray, 32);
            gravity.M32 = BitConverter.ToSingle(numArray, 36);
            gravity.M33 = BitConverter.ToSingle(numArray, 40);
            gravity.M34 = BitConverter.ToSingle(numArray, 44);
            gravity.M41 = BitConverter.ToSingle(numArray, 48);
            gravity.M42 = BitConverter.ToSingle(numArray, 52);
            gravity.M43 = BitConverter.ToSingle(numArray, 56);
            gravity.M44 = BitConverter.ToSingle(numArray, 60);
            return gravity;
        }

        public static Math.AxisAlignedBox ReadAABB(Int64 Address)
        {
            Math.AxisAlignedBox tmp = new Math.AxisAlignedBox();
            byte[] Buffer = new byte[32];
            IntPtr ByteRead;

            Game.Process.ReadProcessMemory(Game.Process.GetInstance.GetHandler(), Address, Buffer, 32, out ByteRead);
            tmp.Min.X = BitConverter.ToSingle(Buffer, (0 * 4));
            tmp.Min.Y = BitConverter.ToSingle(Buffer, (1 * 4));
            tmp.Min.Z = BitConverter.ToSingle(Buffer, (2 * 4));
            tmp.Max.X = BitConverter.ToSingle(Buffer, (4 * 4));
            tmp.Max.Y = BitConverter.ToSingle(Buffer, (5 * 4));
            tmp.Max.Z = BitConverter.ToSingle(Buffer, (6 * 4));
            return tmp;
        }

        public static bool IsValid(Int64 Address)
        {
            return (Address >= 0x10000 && Address < 0x000F000000000000);
        }
    }
}
