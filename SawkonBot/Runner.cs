﻿using System;
using System.Threading;
using System.Windows.Forms;

namespace SawkonBot
{
    class Runner
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private static string fancyTitle = @"   _____                _               ____        _   
  / ____|              | |             |  _ \      | |  
 | (___   __ ___      _| | _____  _ __ | |_) | ___ | |_ 
  \___ \ / _` \ \ /\ / / |/ / _ \| '_ \|  _ < / _ \| __|
  ____) | (_| |\ V  V /|   < (_) | | | | |_) | (_) | |_ 
 |_____/ \__,_| \_/\_/ |_|\_\___/|_| |_|____/ \___/ \__|
                                                        
                                                        ";

        static void Main(string[] args)
        {
            Console.Title = "Sawkon";
            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.Write(fancyTitle);
            Console.SetCursorPosition(10, Console.CursorTop);
            Console.WriteLine("BF4 BOT - Use at your own risk.");
            Console.SetCursorPosition(6, Console.CursorTop);
            Console.WriteLine("Provided to you by sawkon@unknowncheats");
            Console.SetCursorPosition(0, Console.CursorTop + 5);
            Console.Write("Waiting for bf4 ");
            Utils.ConsoleSpinner spinner = new Utils.ConsoleSpinner();
            while (!Game.Process.GetInstance.isRunning())
            {
                spinner.Spin();
                Thread.Sleep(500);
            }
            Console.WriteLine("\rGame is running, have fun!");
            Console.WriteLine("Press HOME to toggle the menu");
            Console.WriteLine("Press END to exit the hack");
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(true);
            Application.Run(new View.Stage());  
        }
    }
}
