﻿using SharpDX;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace SawkonBot.View
{
    class Stage : Form
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private static readonly IntPtr HWND_TOPMOST = new IntPtr(-1);
        private const UInt32 SWP_NOSIZE = 0x0001;
        private const UInt32 SWP_NOMOVE = 0x0002;
        private const UInt32 TOPMOST_FLAGS = SWP_NOMOVE | SWP_NOSIZE;

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int X, int Y, int cx, int cy, uint uFlags);

        [DllImport("dwmapi.dll", CharSet = CharSet.None, ExactSpelling = false)]
        private static extern void DwmExtendFrameIntoClientArea(IntPtr hWnd, ref int[] pMargins);


        public Stage()
        {
            // Setting up the stage as transparent
            this.SuspendLayout();
            this.BackColor = System.Drawing.Color.Black;
            this.DoubleBuffered = true;
            this.TransparencyKey = System.Drawing.Color.Black;
            this.StartPosition = FormStartPosition.Manual;
            this.ForeColor = System.Drawing.SystemColors.ControlText;

            Game.Manager.GetInstance.SetWindow(this);
            this.Load += new EventHandler(this.load);
            this.FormClosing += new FormClosingEventHandler(this.close);
            this.OnResize(null);
            this.ResumeLayout(false);
        }

        public void Tick()
        {
            this.BringToFront();
            this.TopMost = true;

            System.Drawing.Point oldPoint = this.Location;
            Size oldSize = this.Size;

            // Keeps the application with the same size and position of the game
            Game.RECT pRect = Game.Window.GetControlSize();
            Size cSize = new Size();

            cSize.Width = pRect.Right - pRect.Left;
            cSize.Height = pRect.Bottom - pRect.Top;

            this.ClientSize = cSize;

            System.Drawing.Point point = new System.Drawing.Point();
            point.X = pRect.Left;
            point.Y = pRect.Top;
            this.Location = point;

            if(Size != oldSize) {
                Size2 size2 = new Size2();
                size2.Height = Size.Height;
                size2.Width = Size.Width;
                // Changed windows size
                Game.Manager.GetInstance.ResizeWindow(size2);
            }
        }

        protected override void OnResize(EventArgs e)
        {
            int[] width = new int[] { 0, 0, base.Width, base.Height };
            int[] numArray = width;
            DwmExtendFrameIntoClientArea(base.Handle, ref numArray);
        }

        private void load(object sender, EventArgs e)
        {
            SetWindowPos(this.Handle, HWND_TOPMOST, 0, 0, 0, 0, TOPMOST_FLAGS);
            this.WindowState = FormWindowState.Normal;
            this.FormBorderStyle = FormBorderStyle.None;
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer |// this reduce the flicker
                ControlStyles.AllPaintingInWmPaint |
                ControlStyles.DoubleBuffer |
                ControlStyles.UserPaint |
                ControlStyles.Opaque |
                ControlStyles.ResizeRedraw |
                ControlStyles.SupportsTransparentBackColor, true);
        }

        private void close(object sender, EventArgs e)
        {
            Game.Manager.GetInstance.Stop();
        }
    }
}
