﻿using SharpDX;
using SharpDX.Direct2D1;
using SharpDX.DXGI;
using SharpDX.DirectWrite;
using FontFactory = SharpDX.DirectWrite.Factory;
using System;

namespace SawkonBot.View.Helper
{
    partial class DirectxDrawer
    {
        public WindowRenderTarget device { get; set; }

        private TextFormat font;
        private const string fontFamily = "Calibri";
        private const float fontSize = 18.0f;
        private const float fontSizeSmall = 14.0f;

        public DirectxDrawer(Stage stage)
        {
            SharpDX.Direct2D1.Factory factory = new SharpDX.Direct2D1.Factory();
            HwndRenderTargetProperties renderProperties = new HwndRenderTargetProperties()
            {
                Hwnd = stage.Handle,
                PixelSize = new Size2(stage.Width, stage.Height),
                PresentOptions = PresentOptions.None
            };
            this.device = new WindowRenderTarget(factory, new RenderTargetProperties(new PixelFormat(Format.B8G8R8A8_UNorm, SharpDX.Direct2D1.AlphaMode.Premultiplied)), renderProperties);
            FontFactory fontFactory = new FontFactory();
            font = new TextFormat(fontFactory, fontFamily, fontSize);
        }

        public void BeginDraw()
        {
            device.BeginDraw();
            device.Clear(new Color4(0.0f, 0.0f, 0.0f, 0.0f));
        }

        public void EndDraw()
        {
            device.EndDraw();
        }

        public void Resize(Size2 size)
        {
            this.device.Resize(size);
        }
    }
}
