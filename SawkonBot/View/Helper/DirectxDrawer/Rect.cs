﻿using SharpDX;
using SharpDX.Direct2D1;
using System;
using SawkonBot.Game.Engine.Math;
using Matrix4x4 = System.Numerics.Matrix4x4;

namespace SawkonBot.View.Helper
{
    partial class DirectxDrawer
    {
        public void DrawBox(int xAxis, int yAxis, int Width, int Height, Color color)
        {
            RectangleF rectangleF = new RectangleF()
            {
                X = (float)xAxis,
                Y = (float)yAxis,
                Width = (float)Width,
                Height = (float)Height
            };
            device.FillRectangle(rectangleF, new SolidColorBrush(device, color));
        }

        public void DrawRectangle(int xAxis, int yAxis, int Width, int Height, Color color)
        {
            RectangleF rectangleF = new RectangleF()
            {
                X = (float)xAxis,
                Y = (float)yAxis,
                Width = (float)Width,
                Height = (float)Height
            };
            device.DrawRectangle(rectangleF, new SolidColorBrush(device, color));
        }

        public void DrawAABB(Utils.Math.AxisAlignedBox aabb, Vector3 m_Position, float Yaw, Color color)
        {
            float cosY = (float)Math.Cos(Yaw);
            float sinY = (float)Math.Sin(Yaw);

            Vector3 fld = new Vector3(aabb.Min.Z * cosY - aabb.Min.X * sinY, aabb.Min.Y, aabb.Min.X * cosY + aabb.Min.Z * sinY) + m_Position; // 0
            Vector3 brt = new Vector3(aabb.Min.Z * cosY - aabb.Max.X * sinY, aabb.Min.Y, aabb.Max.X * cosY + aabb.Min.Z * sinY) + m_Position; // 1
            Vector3 bld = new Vector3(aabb.Max.Z * cosY - aabb.Max.X * sinY, aabb.Min.Y, aabb.Max.X * cosY + aabb.Max.Z * sinY) + m_Position; // 2
            Vector3 frt = new Vector3(aabb.Max.Z * cosY - aabb.Min.X * sinY, aabb.Min.Y, aabb.Min.X * cosY + aabb.Max.Z * sinY) + m_Position; // 3
            Vector3 frd = new Vector3(aabb.Max.Z * cosY - aabb.Min.X * sinY, aabb.Max.Y, aabb.Min.X * cosY + aabb.Max.Z * sinY) + m_Position; // 4
            Vector3 brb = new Vector3(aabb.Min.Z * cosY - aabb.Min.X * sinY, aabb.Max.Y, aabb.Min.X * cosY + aabb.Min.Z * sinY) + m_Position; // 5
            Vector3 blt = new Vector3(aabb.Min.Z * cosY - aabb.Max.X * sinY, aabb.Max.Y, aabb.Max.X * cosY + aabb.Min.Z * sinY) + m_Position; // 6
            Vector3 flt = new Vector3(aabb.Max.Z * cosY - aabb.Max.X * sinY, aabb.Max.Y, aabb.Max.X * cosY + aabb.Max.Z * sinY) + m_Position; // 7

            if (!Conversion.WorldToScreen(fld, out fld) || !Conversion.WorldToScreen(brt, out brt)
                || !Conversion.WorldToScreen(bld, out bld) || !Conversion.WorldToScreen(frt, out frt)
                || !Conversion.WorldToScreen(frd, out frd) || !Conversion.WorldToScreen(brb, out brb)
                || !Conversion.WorldToScreen(blt, out blt) || !Conversion.WorldToScreen(flt, out flt))
                return;

            DrawLine(fld, brt, color);
            DrawLine(brb, blt, color);
            DrawLine(fld, brb, color);
            DrawLine(brt, blt, color);

            DrawLine(frt, bld, color);
            DrawLine(frd, flt, color);
            DrawLine(frt, frd, color);
            DrawLine(bld, flt, color);

            DrawLine(frt, fld, color);
            DrawLine(frd, brb, color);
            DrawLine(brt, bld, color);
            DrawLine(blt, flt, color);
        }

        public void DrawAABB(Game.Model.VehicleAABBScreen aabbscreen, Color color)
        {
            if(aabbscreen == default(Game.Model.VehicleAABBScreen))
            {
                return;
            }
            float strokeWidth = 1.5f;
            this.DrawLine(aabbscreen._Screen1, aabbscreen._Screen8, color, strokeWidth);
            this.DrawLine(aabbscreen._Screen8, aabbscreen._Screen4, color, strokeWidth);
            this.DrawLine(aabbscreen._Screen4, aabbscreen._Screen5, color, strokeWidth);
            this.DrawLine(aabbscreen._Screen5, aabbscreen._Screen1, color, strokeWidth);
            this.DrawLine(aabbscreen._Screen3, aabbscreen._Screen7, color, strokeWidth);
            this.DrawLine(aabbscreen._Screen7, aabbscreen._Screen2, color, strokeWidth);
            this.DrawLine(aabbscreen._Screen2, aabbscreen._Screen6, color, strokeWidth);
            this.DrawLine(aabbscreen._Screen6, aabbscreen._Screen3, color, strokeWidth);
            this.DrawLine(aabbscreen._Screen1, aabbscreen._Screen3, color, strokeWidth);
            this.DrawLine(aabbscreen._Screen5, aabbscreen._Screen6, color, strokeWidth);
            this.DrawLine(aabbscreen._Screen8, aabbscreen._Screen7, color, strokeWidth);
            this.DrawLine(aabbscreen._Screen4, aabbscreen._Screen2, color, strokeWidth);
        }

        public void DrawBone(Game.Engine.ClientPlayer.Bone bone, Color color)
        {
            Game.Engine.ClientPlayer.Bone convertedBone = bone.ConvertToScreen();
            if (convertedBone.Head.X != 0 && convertedBone.Head.Y != 0 && convertedBone.Head.Z != 0 && convertedBone.Spine.X != 0 && convertedBone.Spine.Y != 0 && convertedBone.Spine.Z != 0)
            {
                DrawLine((int)convertedBone.Head.X, (int)convertedBone.Head.Y, (int)convertedBone.Neck.X, (int)convertedBone.Neck.Y, color, 4);

                DrawLine((int)convertedBone.Neck.X, (int)convertedBone.Neck.Y, (int)convertedBone.LeftShoulder.X, (int)convertedBone.LeftShoulder.Y, color, 2);
                DrawLine((int)convertedBone.Neck.X, (int)convertedBone.Neck.Y, (int)convertedBone.RightShoulder.X, (int)convertedBone.RightShoulder.Y, color, 2);

                DrawLine((int)convertedBone.LeftShoulder.X, (int)convertedBone.LeftShoulder.Y, (int)convertedBone.LeftElbow.X, (int)convertedBone.LeftElbow.Y, color, 2);
                DrawLine((int)convertedBone.RightShoulder.X, (int)convertedBone.RightShoulder.Y, (int)convertedBone.RightElbow.X, (int)convertedBone.RightElbow.Y, color, 2);

                DrawLine((int)convertedBone.LeftElbow.X, (int)convertedBone.LeftElbow.Y, (int)convertedBone.LeftHand.X, (int)convertedBone.LeftHand.Y, color, 2);
                DrawLine((int)convertedBone.RightElbow.X, (int)convertedBone.RightElbow.Y, (int)convertedBone.RightHand.X, (int)convertedBone.RightHand.Y, color, 2);

                DrawLine((int)convertedBone.Neck.X, (int)convertedBone.Neck.Y, (int)convertedBone.Spine.X, (int)convertedBone.Spine.Y, color, 2);

                DrawLine((int)convertedBone.Spine.X, (int)convertedBone.Spine.Y, (int)convertedBone.LeftKnee.X, (int)convertedBone.LeftKnee.Y, color, 2);
                DrawLine((int)convertedBone.Spine.X, (int)convertedBone.Spine.Y, (int)convertedBone.RightKnee.X, (int)convertedBone.RightKnee.Y, color, 2);

                DrawLine((int)convertedBone.LeftKnee.X, (int)convertedBone.LeftKnee.Y, (int)convertedBone.LeftFoot.X, (int)convertedBone.LeftFoot.Y, color, 2);
                DrawLine((int)convertedBone.RightKnee.X, (int)convertedBone.RightKnee.Y, (int)convertedBone.RightFoot.X, (int)convertedBone.RightFoot.Y, color, 2);
            }
        }

        public void DrawHealthBar(int X, int Y, int Width, int Height, int Health, int MaxHealth)
        {
            if (MaxHealth < Health) MaxHealth = 100;
            Color color = Color.Red;
            int progress = (int)((float)Health / ((float)MaxHealth / 100));
            int width = (int)((float)Width / 100 * progress);

            if (progress > 0 && progress < 20) color = color = new Color(255, 0, 0, 255);
            if (progress >= 20) color = color = new Color(255, 153, 0, 255);
            if (progress >= 40) color = new Color(229, 229, 0, 255);
            if (progress >= 60) color = new Color(255, 204, 0, 255);
            if (progress >= 80) color = new Color(105, 167, 78, 255);

            DrawFillRect(X, Y - 1, Width + 1, Height + 2, Color.DarkBlue);
            DrawFillRect(X + 1, Y, width - 1, Height, color);
        }

        public void DrawFillRect(int X, int Y, int Width, int Height, Color color)
        {
            RectangleF rect = new RectangleF();
            rect.X = X;
            rect.Y = Y;
            rect.Width = Width;
            rect.Height = Height;
            device.FillRectangle(rect, new SolidColorBrush(this.device, color));
        }
    }
}
