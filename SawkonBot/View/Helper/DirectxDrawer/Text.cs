﻿using SharpDX;
using SharpDX.Direct2D1;
using SharpDX.DXGI;
using SharpDX.DirectWrite;
using FontFactory = SharpDX.DirectWrite.Factory;


namespace SawkonBot.View.Helper
{
    partial class DirectxDrawer
    {
        // Draw Text
        public void DrawText(int X, int Y, string text, Color color)
        {
            device.DrawText(text, font, new RectangleF(X, Y, font.FontSize * text.Length, font.FontSize), new SolidColorBrush(this.device, color));
        }

        public void DrawText(int X, int Y, string text, Color color, TextFormat font)
        {
            device.DrawText(text, font, new RectangleF(X, Y, font.FontSize * text.Length, font.FontSize), new SolidColorBrush(this.device, color));
        }

        public void DrawText(string message, int x, int y, int width, int height, bool center, Color color, Color shadow)
        {
            FontFactory fontFactory = new FontFactory();
            TextFormat _font = new TextFormat(fontFactory, "Verdana", 14.0f);

            SolidColorBrush solidColorBrush = new SolidColorBrush(this.device, color);
            if (center)
            {
                x = (int)((float)x - (float)message.Length * _font.FontSize / 2f + 0.5f);
                y = (int)((float)y + 3f);
            }
            RectangleF rectangleF = new SharpDX.RectangleF()
            {
                Height = (float)height,
                Width = (float)width,
                X = (float)x,
                Y = (float)y
            };
            solidColorBrush.Color = shadow;
            rectangleF.X = (float)(x + 1);
            rectangleF.Y = (float)y;
            device.DrawText(message, _font, rectangleF, solidColorBrush);
            rectangleF.X = (float)x;
            rectangleF.Y = (float)(y + 1);
            device.DrawText(message, _font, rectangleF, solidColorBrush);
            rectangleF.X = (float)(x - 1);
            rectangleF.Y = (float)y;
            device.DrawText(message, _font, rectangleF, solidColorBrush);
            rectangleF.X = (float)x;
            rectangleF.Y = (float)(y - 1);
            device.DrawText(message, _font, rectangleF, solidColorBrush);
            rectangleF.X = (float)x;
            rectangleF.Y = (float)y;
            solidColorBrush.Color = color;
            device.DrawText(message, _font, rectangleF, solidColorBrush);
        }
    }
}
