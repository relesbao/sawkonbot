﻿using SharpDX;
using SharpDX.Direct2D1;
using SharpDX.DXGI;
using SharpDX.DirectWrite;

namespace SawkonBot.View.Helper
{
    partial class DirectxDrawer
    {
        public void DrawLine(Vector3 w2s, Vector3 _w2s, Color color)
        {
            device.DrawLine(new Vector2(w2s.X, w2s.Y), new Vector2(_w2s.X, _w2s.Y), new SolidColorBrush(this.device, color));
        }

        public void DrawLine(Vector3 w2s, Vector3 _w2s, Color color, float strokeWidth)
        {
            device.DrawLine(new Vector2(w2s.X, w2s.Y), new Vector2(_w2s.X, _w2s.Y), new SolidColorBrush(this.device, color), strokeWidth);
        }

        public void DrawLine(int X, int Y, int XX, int YY, Color color, float strokeWidth)
        {
            device.DrawLine(new Vector2(X, Y), new Vector2(XX, YY), new SolidColorBrush(this.device, color), strokeWidth);
        }

        public void DrawLine(Vector2 point0, Vector2 point1, Color color, float strokeWidth)
        {
            device.DrawLine(point0, point1, new SolidColorBrush(this.device, color), strokeWidth);
        }
    }
}
