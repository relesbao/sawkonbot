﻿using SharpDX;
using SharpDX.Direct2D1;

namespace SawkonBot.View.Helper
{
    partial class DirectxDrawer
    {
        public void DrawCircle(int X, int Y, int W, Color color)
        {
            this.device.DrawEllipse(new Ellipse(new Vector2((float)X, (float)Y), (float)W, (float)W), new SolidColorBrush(this.device, color));
        }
    }
}
